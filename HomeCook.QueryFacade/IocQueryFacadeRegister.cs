using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using StructureMap;

namespace HomeCook.QueryFacade
{
    public class IocQueryFacadeRegister : Registry
    {
        public IocQueryFacadeRegister()
        {
            For<IQueryFacade<Right>>().Use<MySqlQueryFacade<Right>>();
            For<IQueryFacade<Role>>().Use<MySqlQueryFacade<Role>>();
            For<IQueryFacade<User>>().Use<MySqlQueryFacade<User>>();
            For<IQueryFacade<FileInfo>>().Use<MySqlQueryFacade<FileInfo>>();
        }
    }
}
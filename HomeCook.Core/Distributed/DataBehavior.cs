﻿namespace HomeCook.Core.Distributed
{
    public enum DataBehavior
    {
        Queue,
        Broadcast,
        Stack
    }
}
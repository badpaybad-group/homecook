﻿using System;
using Newtonsoft.Json;

namespace HomeCook.Core.Distributed
{
    public sealed class DistributedCommand 
    {
        public   Guid Id { get;  set; }
        public   DataBehavior DataBehavior { get; set; }

        public object Data { get; set; }

        public   string DataType { get; set; }

        public DistributedCommand() { } 

        public DistributedCommand(object data, DataBehavior dataBehavior = DataBehavior.Queue)
        {
            Id = Guid.NewGuid();
            Data = data;
            DataType = data.GetType().AssemblyQualifiedName;
            DataBehavior = dataBehavior;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public DistributedCommand FromJson(string json)  
        {
            return JsonConvert.DeserializeObject<DistributedCommand>(json);
        }
    }
}
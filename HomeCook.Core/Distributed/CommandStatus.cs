﻿namespace HomeCook.Core.Distributed
{
    public enum CommandStatus
    {
        None,
        Pushed,
        Pedding,
        Success,
        Error
    }
}

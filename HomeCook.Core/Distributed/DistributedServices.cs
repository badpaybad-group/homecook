using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HomeCook.Core.Redis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;

namespace HomeCook.Core.Distributed
{
    public static class DistributedServices //: IDistributedServices
    {
        private static Thread _commandQueue;
        private static Thread _findingResendCmd;

        const string juljulCommandLogPushed = "juljul_command_log_pushed";
        const string juljulCommandLogPendding = "juljul_command_log_pendding";
        const string juljulCommandLogError = "juljul_command_log_error";
        const string juljulCommandLogSucess = "juljul_command_log_sucess";
        const string juljulChannelByDataType = "juljul_channel_log_by_data_type";

        const string juljulCommandQueueWaitToSend = "juljul_command_queue_wait_to_send";

        const string juljulLock = "juljul_lock";

        static DistributedServices()
        {
            TryDequeueToPublishCmd();

            TryRepublishCmd();
        }

        private static void TryDequeueToPublishCmd()
        {
            _commandQueue = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        var cmds = new List<string>();
                        var counter = 0;
                        while (true)
                        {
                            var redisValue = RedisHelper.RedisDatabase.ListLeftPop(juljulCommandQueueWaitToSend);
                            if (!redisValue.HasValue || counter > 100) break;

                            counter++;
                            cmds.Add(redisValue);
                        }
                        //ThreadPool.QueueUserWorkItem((o)=>
                        Task.Run(() =>
                        {
                            foreach (var redisValue in cmds)
                            {
                                var cmd = JsonConvert.DeserializeObject<DistributedCommand>(redisValue);

                                var channel = cmd.DataType;
                                var dataKeyStored = cmd.DataBehavior + channel;
                                switch (cmd.DataBehavior)
                                {
                                    case DataBehavior.Queue:
                                    case DataBehavior.Stack:
                                        RedisHelper.RedisDatabase.ListRightPush(dataKeyStored, redisValue);
                                        break;
                                    case DataBehavior.Broadcast:
                                        RedisHelper.RedisDatabase.HashSet(dataKeyStored, cmd.Id.ToString(),
                                            redisValue);
                                        break;
                                }

                                RedisHelper.RedisDatabase.Publish(channel, redisValue);

                                LogPushed(channel, cmd.Id, redisValue);
#if DEBUG
                                //Console.WriteLine("Pushed:" + redisValue);
#endif
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    finally
                    {
                        Thread.Sleep(100);
                    }
                }
            });

            _commandQueue.Start();
        }

        private static void TryRepublishCmd()
        {
            _findingResendCmd = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        var lck = RedisHelper.RedisDatabase.StringGet(juljulLock);
                        if (lck.HasValue && (bool) lck) continue;

                        RedisHelper.RedisDatabase.StringSet(juljulLock, true);
                        RedisHelper.RedisDatabase.KeyExpire(juljulLock, new TimeSpan(0, 0, 1, 0));

                        //todo: when huge amount command pushed. we will be re-thinking
                        var allCmdPushed =
                            RedisHelper.RedisDatabase.HashGetAll(juljulCommandLogPushed);

                        foreach (var cmdP in allCmdPushed)
                        {
                            var pending = RedisHelper.RedisDatabase.HashGet(juljulCommandLogPendding, cmdP.Name);

                            if (pending.HasValue) continue;

                            var redisValue = cmdP.Value;

                            RedisHelper.RedisDatabase.ListRightPush(juljulCommandQueueWaitToSend, redisValue);
#if DEBUG
                            //Console.WriteLine("--repush cmd: " + cmdP.Name + " --data: " + redisValue);
#endif
                        }

                        allCmdPushed = null;

                        RedisHelper.RedisDatabase.KeyDelete(juljulLock);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    finally
                    {
                        Thread.Sleep(5000);
                    }
                }
            });

            _findingResendCmd.Start();
        }

        public static void Publish(DistributedCommand cmd,bool retry=false, Action<Exception> errorCallback = null) 
        {
            try
            {
                var redisValue = cmd.ToJson();

                RedisHelper.RedisDatabase.ListRightPush(juljulCommandQueueWaitToSend, redisValue);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                if (errorCallback != null) errorCallback(ex);
                Thread.Sleep(1000);
                if(retry)
                { Publish(cmd, true, errorCallback);}
            }
        }
        
        public static void Subscribe(Type dataType, Action<DistributedCommand> callBack, Action<Exception> errorCallback = null)
            
        {
            try
            {
                var redisChannel = dataType.AssemblyQualifiedName;
                RedisHelper.RedisSubscriber.Subscribe(redisChannel, (channel, value) =>
                {
                    try
                    {
                        var cmd = JsonConvert.DeserializeObject<DistributedCommand>(value);

                        LogPendding(cmd.Id, value);

                        TaskTryRun(cmd.Id, value, () =>
                        {
                            var isDone = false;
                            switch (cmd.DataBehavior)
                            {
                                case DataBehavior.Queue:
                                    while (true)
                                    {
                                        var qv = RedisHelper.RedisDatabase.ListLeftPop(cmd.DataBehavior + redisChannel);
                                        if (!qv.HasValue) break;
                                        try
                                        {
                                            var data = JsonConvert.DeserializeObject<DistributedCommand>(qv);
                                           
                                                var jobj = data.Data as JObject;
                                                if (jobj != null)
                                                {

                                                    data.Data = jobj.ToObject(dataType);
                                                    //JsonConvert.DeserializeObject(jobj.ToString(), dataType);
                                                }
                                       
                                            callBack(data);
                                            isDone = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            RedisHelper.RedisDatabase.ListRightPush(cmd.DataBehavior + redisChannel,qv);
                                        }
                                      
                                    }

                                    break;
                                case DataBehavior.Stack:
                                    while (true)
                                    {
                                        var sv =
                                            RedisHelper.RedisDatabase.ListRightPop(cmd.DataBehavior + redisChannel);
                                        if (!sv.HasValue) break;
                                        try
                                        {
                                            var data = JsonConvert.DeserializeObject<DistributedCommand>(sv);
                                            var jobj = data.Data as JObject;
                                            if (jobj != null)
                                            {

                                                data.Data = jobj.ToObject(dataType);
                                                //JsonConvert.DeserializeObject(jobj.ToString(), dataType);
                                            }
                                            callBack(data);
                                            isDone = true;
                                        }
                                        catch
                                        {
                                            RedisHelper.RedisDatabase.ListRightPush(cmd.DataBehavior + redisChannel, sv);
                                        }
                                    }

                                    break;
                                case DataBehavior.Broadcast:
                                    var da = RedisHelper.RedisDatabase.HashGet(cmd.DataBehavior + redisChannel,
                                        cmd.Id.ToString());
                                    if (da.HasValue)
                                    {
                                        try
                                        {
                                            var data = JsonConvert.DeserializeObject<DistributedCommand>(da);
                                            var jobj = data.Data as JObject;
                                            if (jobj != null)
                                            {

                                                data.Data = jobj.ToObject(dataType);
                                                //JsonConvert.DeserializeObject(jobj.ToString(), dataType);
                                            }
                                            callBack(data);
                                            isDone = true;
                                        }
                                        catch 
                                        {
                                          
                                        }
                                   
                                    }
                                    break;
                            }
                            if (!isDone) return;

                            LogSuccess(cmd.Id, cmd.ToJson());
#if DEBUG
                            //Console.WriteLine("Done:" + value);
#endif
                        }, errorCallback);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        if (errorCallback != null) errorCallback(ex);
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                if (errorCallback != null) errorCallback(ex);
                Thread.Sleep(1000);
                Subscribe(dataType, callBack,errorCallback );
            }
        }

        public static void Unsubscribe<T>(Action<Exception> errorCallback = null)
        {
            try
            {
                var redisChannel = typeof(T).AssemblyQualifiedName;
                RedisHelper.RedisSubscriber.Unsubscribe(redisChannel);
            }
            catch (Exception ex)
            {
                if (errorCallback != null) errorCallback(ex);
            }
        }

        public static void Unsubscribe(Type type, Action<Exception> errorCallback = null)
        {
            try
            {
                var redisChannel = type.AssemblyQualifiedName;
                RedisHelper.RedisSubscriber.Unsubscribe(redisChannel);
            }
            catch (Exception ex)
            {
                if (errorCallback != null) errorCallback(ex);
            }
        }

        private static void LogPendding(Guid cmdId, RedisValue cmdJson)
        {
            RedisHelper.RedisDatabase.HashSet(juljulCommandLogPendding, cmdId.ToString(), cmdJson);
        }

        static  void LogPushed(string dataType, Guid cmdId, RedisValue cmdJson)
        {
            RedisHelper.RedisDatabase.HashSet(juljulChannelByDataType, dataType, cmdJson);
            RedisHelper.RedisDatabase.HashSet(juljulCommandLogPushed, cmdId.ToString(), cmdJson);
        }

       static void LogError(Guid cmdId, RedisValue cmdJson) 
        {
            RedisHelper.RedisDatabase.HashSet(juljulCommandLogError, cmdId.ToString(), cmdJson);
        }

      static  void LogSuccess(Guid cmdId, RedisValue cmdJson) 
        {
            var cmdid = cmdId.ToString();
            RedisHelper.RedisDatabase.HashSet(juljulCommandLogSucess, cmdid, cmdJson);
            var error = RedisHelper.RedisDatabase.HashGet(juljulCommandLogError, cmdid);
            if (error.HasValue)
            {
                RedisHelper.RedisDatabase.HashDelete(juljulCommandLogError, cmdid);
            }
        }

      static  void TaskTryRun(Guid cmdId, string cmdJson, Action a, Action<Exception> error) 
        {
            Task.Run(() =>
            //ThreadPool.QueueUserWorkItem((o)=>
            {
                try
                {
                    a();
                }
                catch (Exception ex)
                {
                    LogError(cmdId, cmdJson);

                    if (error != null) error(ex);
                    Console.WriteLine(ex);
                }
            });
        }

        //void TryCatchLog(Action a)
        //{
        //    try
        //    {
        //        a();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}
    }
}
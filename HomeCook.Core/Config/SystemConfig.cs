﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeCook.Core.Config
{
  public  class SystemConfig
    {
      static string GetValue(string key, bool allowEmpty= false)
      {
            var temp = ConfigurationManager.AppSettings[key];
            if(!allowEmpty)
            if (string.IsNullOrEmpty(temp)) throw new KeyNotFoundException(key+ " not found in appSettings");
          return temp;
      }

        public static string UrlLogin
        {
            get {return GetValue("UrlLogin"); }
        }

        public static string AdminUsername
        {
            get { return "admin"; }
        }

        public static string AdminPassword
        {
            get { return "123456"; }
        }

       

        public class ImageServer
        {
            public static string ImageDomain
            {
                get { return GetValue("ImageServer_ImageDomain",true); }
            }


            public static string ImagePhysicalPath
            {
                get { return GetValue("ImageServer_ImagePhysicalPath", true); }
            }


            public static string ImageVirtualPath
            {
                get { return GetValue("ImageServer_ImageVirtualPath", true); }
            }
        }

    }
}

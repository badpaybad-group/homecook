﻿namespace HomeCook.Core.Cqrs
{
    public interface ICommand
    {
        string ClientId { get; set; }
    }
    public interface IEvent
    {
        string ClientId { get; set; }
        int Version { get; set; }
    }
}
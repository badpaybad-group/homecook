using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using HomeCook.Core.Distributed;

namespace HomeCook.Core.Cqrs
{
    public static class CqrsEventHandlerManager
    {
        public static void Boot()
        {
            //var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            //foreach (var assembly in assemblies)
            //{
            //    Register(assembly);
            //}

            lock (_mapEventHandlers)
            {
                _mapEventHandlers.Clear();
            }

            List<Assembly> allAssemblies = new List<Assembly>();
            var executingAssembly = Assembly.GetExecutingAssembly();

            string path = Path.GetDirectoryName(executingAssembly.Location);

            foreach (string dll in Directory.GetFiles(path, "*.dll"))
                allAssemblies.Add(Assembly.LoadFile(dll));


            foreach (var assembly in allAssemblies)
            {
                //make sure event handle run done 
                //then process and workfollow stuff do late
                RegisterHandle(assembly);
                RegisterProcessOrWorkfolowHandle(assembly);
            }

            //if (_mapHandlers.Count == 0)
            //{
            //    throw new NotImplementedException("No ICqrsHandler implemented");
            //}

            foreach (var map in _mapEventHandlers)
            {
                DistributedServices.Subscribe(map.Key, (m) =>
                {
                    foreach (var h in map.Value)
                    {
                        h(m);
                    }
                });
            }
        }

        static Dictionary<Type, List<Action<DistributedCommand>>> _mapEventHandlers =
            new Dictionary<Type, List<Action<DistributedCommand>>>();
        
        public static void RegisterHandle(Assembly assembly)
        {
            try
            {
                var allTypes = assembly.GetTypes();
                var listCqrsHandler = allTypes.Where(t => typeof(ICqrsEventHandler).IsAssignableFrom(t)
                                                          && t.IsClass && !t.IsAbstract).ToList();

                foreach (var handlerType in listCqrsHandler)
                {
                    var cqrsHandler = (ICqrsEventHandler) Activator.CreateInstance(handlerType);
                    if (cqrsHandler == null) continue;

                    MethodInfo[] allMethod = cqrsHandler.GetType()
                        .GetMethods(BindingFlags.Public | BindingFlags.Instance);

                    foreach (var mi in allMethod)
                    {
                        if (!mi.Name.Equals("handle", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var pParameterType = mi.GetParameters().SingleOrDefault().ParameterType;

                        void InvokeHandle(DistributedCommand msg)
                        {
                            var cqrsEvent = msg.Data as IEvent;
                            if (cqrsEvent == null)
                            {
                                throw new NotSupportedException("msg.Data is not inherit IEvent");
                            }

                            try
                            {
                                mi.Invoke(cqrsHandler, new object[] {cqrsEvent});
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }

                        List<Action<DistributedCommand>> actions = null;
                        if (_mapEventHandlers.TryGetValue(pParameterType, out actions))
                        {
                            actions.Add(InvokeHandle);
                        }
                        else
                        {
                            actions = new List<Action<DistributedCommand>>();
                            actions.Add(InvokeHandle);
                        }
                        _mapEventHandlers[pParameterType] = actions;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error load assembly: " + assembly + " " + e.Message);
            }
        }


        public static void RegisterProcessOrWorkfolowHandle(Assembly assembly)
        {
            try
            {
                var allTypes = assembly.GetTypes();
                var listCqrsHandler = allTypes.Where(t => typeof(ICqrsProcessOrWorkfollowManager).IsAssignableFrom(t)
                                                          && t.IsClass && !t.IsAbstract).ToList();

                foreach (var handlerType in listCqrsHandler)
                {
                    var cqrsHandler = (ICqrsProcessOrWorkfollowManager) Activator.CreateInstance(handlerType);
                    if (cqrsHandler == null) continue;

                    MethodInfo[] allMethod = cqrsHandler.GetType()
                        .GetMethods(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var mi in allMethod)
                    {
                        if (!mi.Name.Equals("handle", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var pParameterType = mi.GetParameters().SingleOrDefault().ParameterType;

                        void InvokeHandle(DistributedCommand msg)
                        {
                            var cqrsEvent = msg.Data as IEvent;
                            if (cqrsEvent == null)
                            {
                                throw new NotSupportedException("msg.Data is not inherit IEvent");
                            }

                            try
                            {
                                mi.Invoke(cqrsHandler, new object[] {cqrsEvent});
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }

                        List<Action<DistributedCommand>> actions = null;
                        if (_mapEventHandlers.TryGetValue(pParameterType, out actions))
                        {
                            actions.Add(InvokeHandle);
                        }
                        else
                        {
                            actions = new List<Action<DistributedCommand>>();
                            actions.Add(InvokeHandle);
                        }
                        _mapEventHandlers[pParameterType] = actions;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error load assembly: " + assembly + " " + e.Message);
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using HomeCook.Core.Distributed;
using Newtonsoft.Json.Linq;

namespace HomeCook.Core.Cqrs
{
    public static class CqrsCommandHandlerManager
    {
        public static void Boot()
        {
            //var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            //foreach (var assembly in assemblies)
            //{
            //    Register(assembly);
            //}

            lock (_mapCommandHandlers)
            {
                _mapCommandHandlers.Clear();
            }

            List<Assembly> allAssemblies = new List<Assembly>();
            var executingAssembly = Assembly.GetExecutingAssembly();

            string path = Path.GetDirectoryName(executingAssembly.Location);

            foreach (string dll in Directory.GetFiles(path, "*.dll"))
                allAssemblies.Add(Assembly.LoadFile(dll));


            foreach (var assembly in allAssemblies)
            {
                RegisterHandle(assembly);
            }

            //if (_mapHandlers.Count == 0)
            //{
            //    throw new NotImplementedException("No ICqrsHandler implemented");
            //}
            foreach (var m in _mapCommandHandlers)
            {
                DistributedServices.Subscribe(m.Key, m.Value);
            }
        }

        static Dictionary<Type,Action<DistributedCommand>> _mapCommandHandlers=new Dictionary<Type, Action<DistributedCommand>>();

        public static void RegisterHandle(Assembly assembly)
        {
            try
            {
                var allTypes = assembly.GetTypes();
                var listHandler = allTypes.Where(t => typeof(ICqrsCommandHandler).IsAssignableFrom(t)
                                                        && t.IsClass && !t.IsAbstract).ToList();

                foreach (var handlerType in listHandler)
                {
                    var cqrsHandler = (ICqrsCommandHandler) Activator.CreateInstance(handlerType);
                    if (cqrsHandler == null) continue;

                    MethodInfo[] allMethod = cqrsHandler.GetType()
                        .GetMethods(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var mi in allMethod)
                    {
                        if (!mi.Name.Equals("handle", StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var pParameterType = mi.GetParameters().SingleOrDefault().ParameterType;

                        void InvokeHandle(DistributedCommand msg)
                        {
                            var cqrsCmd = msg.Data as ICommand;
                            if (cqrsCmd == null)
                                throw new NotSupportedException("msg.Data is not inherit ICommand");

                            try
                            {
                                mi.Invoke(cqrsHandler, new object[] { cqrsCmd });
                                CqrsNotificationServices.Push(new CommandResultNotificationMessage(cqrsCmd)
                                {
                                    IsSuccess = true,
                                    Message = ""
                                });
                            }
                            catch (Exception ex)
                            {
                                CqrsNotificationServices.Push(new CommandResultNotificationMessage(cqrsCmd)
                                {
                                    IsSuccess = false,
                                    Message = ex.Message
                                });
                            }
                        }

                        //signle command single handle
                        _mapCommandHandlers.Add(pParameterType, InvokeHandle);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error load assembly: "+ assembly+" "+ e.Message );
            }
        }
    }
}
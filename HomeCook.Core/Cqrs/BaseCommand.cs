using System;

namespace HomeCook.Core.Cqrs
{
    public class BaseCommand : ICommand
    {
        public BaseCommand(Guid id)
        {
            Id = id;
        }
        public Guid Id { get;  }
        public Guid LanguageId { get; set; }
        public string ClientId { get; set; }
    }

    public class BaseCommandWithVersion : BaseCommand
    {
        public int CurrentVersion { get; }

        public BaseCommandWithVersion(Guid id, int currentVersion) : base(id)
        {
            this.CurrentVersion = currentVersion;
        }
    }
}
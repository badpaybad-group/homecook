using System;

namespace HomeCook.Core.Cqrs
{
    public class BaseEvent:IEvent
    {
        public BaseEvent(Guid id)
        {
            Id = id;
        }
        public Guid Id { get;  }
        public Guid LanguageId { get; set; }
        public string ClientId { get; set; }
        public int Version { get; set; }
    }
}
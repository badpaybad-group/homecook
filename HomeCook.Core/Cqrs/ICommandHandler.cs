namespace HomeCook.Core.Cqrs
{
    public interface ICommandHandler<T> : ICqrsCommandHandler where T : class, ICommand
    {
        void Handle(T cmd);
    }

    public interface ICqrsCommandHandler
    {
    }
}
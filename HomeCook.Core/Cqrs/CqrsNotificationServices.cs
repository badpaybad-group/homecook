using System;
using HomeCook.Core.Redis;
using Newtonsoft.Json;

namespace HomeCook.Core.Cqrs
{
    public class CqrsNotificationServices
    {
        public const string SubscribeName = "CqrsNotificationServices";

        public static void Push(CommandResultNotificationMessage msg)
        {
            var cmdClientId = msg.CmdClientId;
            if (string.IsNullOrEmpty(cmdClientId)) return;
            var keyChannel = SubscribeName + "_" + cmdClientId;
            keyChannel = keyChannel.ToLower();
            var msgJsonData = JsonConvert.SerializeObject(msg);
            RedisHelper.RedisSubscriber.Publish(keyChannel, msgJsonData);
        }
        
        public static void UnSubscribe(string cmdClientId)
        {
            if (string.IsNullOrEmpty(cmdClientId)) return;
            var keyChannel = SubscribeName + "_" + cmdClientId;
            keyChannel = keyChannel.ToLower();
            RedisHelper.RedisSubscriber.Unsubscribe(keyChannel);
        }

        public static void Subscribe(string cmdClientId, Action<CommandResultNotificationMessage> callBack)
        {
            if (string.IsNullOrEmpty(cmdClientId)) return;
            var keyChannel = SubscribeName + "_" + cmdClientId;
            keyChannel = keyChannel.ToLower();
            RedisHelper.RedisSubscriber.Unsubscribe(keyChannel);

            RedisHelper.RedisSubscriber.Subscribe(keyChannel, (redisChannel, value) =>
            {
                var msg = JsonConvert.DeserializeObject<CommandResultNotificationMessage>(value);
                callBack(msg);
            });
        }
    }
}
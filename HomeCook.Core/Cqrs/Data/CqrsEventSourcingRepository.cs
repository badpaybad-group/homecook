using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.Serialization;
using HomeCook.Core.Data;
using HomeCook.Core.Distributed;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HomeCook.Core.Cqrs.Data
{
    public class CqrsEventSourcingRepository<TAggregate> : ICqrsEventSourcingRepository<TAggregate>
        where TAggregate : AggregateRoot, new()
    {
        EventSourcingDbContext _eventSourcingDbContext = new EventSourcingDbContext();

        private static IDatabaseInitializer<EventSourcingDbContext> _databaseInitializer =
            new CreateDatabaseIfNotExists<EventSourcingDbContext>();

        public CqrsEventSourcingRepository()
        {
            _databaseInitializer.InitializeDatabase(_eventSourcingDbContext);
        }

        public TAggregate Get(Guid id)
        {
            var eventsHistory = _eventSourcingDbContext.EventSoucings.AsNoTracking()
                .Where(i => i.Id.Equals(id)
                    //&& i.AggregateType.Equals(typeof(TAggregate).FullName,StringComparison.OrdinalIgnoreCase)
                ).OrderBy(i => i.Version).ThenBy(i => i.CreatedDate).ToList();

            if (eventsHistory.Any() == false)
                throw new AggregateNotFoundException("Not found AggregateType: " + typeof(TAggregate).FullName +
                                                     " with Id: " + id);

            TAggregate a = new TAggregate();

            List<BaseEvent> convertedEvents = new List<BaseEvent>();

            foreach (var e in eventsHistory)
            {
                try
                {
                    var jobj = JsonConvert.DeserializeObject(e.EventData) as JObject;
                    var objectType = Type.GetType(e.EventType, false, true);

                    if (objectType == null || jobj == null) continue;

                    var o = jobj.ToObject(objectType);
                    convertedEvents.Add((BaseEvent) o);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            a.LoadFromHistory(convertedEvents);

            return a;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregate"></param>
        /// <param name="expectedVersion">-1: automatic get lastest version</param>
        public void Save(TAggregate aggregate, int expectedVersion=-1)
        {
            List<EventSourcing> eventChanges = new List<EventSourcing>();

            var aggregateChanges = aggregate.Changes.ToList();

            var lastVersion = 0;

            using (var db = new EventSourcingDbContext())
            {
                var temp = db.EventSoucings.Where(i => i.Id == aggregate.Id).Max(i => i.Version);
                lastVersion = temp == null ? 0 : temp;
            }
            
            if (expectedVersion >= 0)
            {
                if (lastVersion != expectedVersion)
                {
                    throw new ConflickVersion(
                        string.Format(
                            "Version conflick for AggregateType: {0} with Id: {1} Latest version: {2} Expected version: {3}"
                            , aggregate.GetType().FullName, aggregate.Id, lastVersion, expectedVersion));
                }
                lastVersion = expectedVersion;
            }

            foreach (var e in aggregateChanges)
            {
                lastVersion++;
                e.Version = lastVersion;
                //build event data add to event store db
                eventChanges.Add(new EventSourcing()
                {
                    Id = aggregate.Id,
                    AggregateType = typeof(TAggregate).AssemblyQualifiedName,
                    EventData = JsonConvert.SerializeObject(e),
                    EventType = e.GetType().AssemblyQualifiedName,
                    Version = lastVersion,
                    CreatedDate = DateTime.Now
                });
            }

            //save to event store db
            using (var db = new EventSourcingDbContext())
            {
                db.EventSoucings.AddRange(eventChanges);
                db.SaveChanges();
            }

            foreach (var e in eventChanges)
            {
                //publish event  
                DistributedServices.Publish(new DistributedCommand(e), true);
            }
        }

        public void CreateNew(TAggregate aggregate)
        {
            Save(aggregate, -1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="aggregateActionsApplyBeforeSave">Action before save</param>
        /// <param name="expectedVersion">-1: automatic get lastest version</param>
        public void Save(Guid id
            , Action<TAggregate> aggregateActionsApplyBeforeSave
            , int expectedVersion=-1)
        {
            var aggregate = Get(id);
            aggregateActionsApplyBeforeSave(aggregate);
            Save(aggregate, expectedVersion);
        }
    }

    public class ConflickVersion : Exception
    {
        public ConflickVersion()
        {
        }

        public ConflickVersion(string message) : base(message)
        {
        }

        public ConflickVersion(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConflickVersion(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class AggregateNotFoundException : Exception
    {
        public AggregateNotFoundException()
        {
        }

        public AggregateNotFoundException(string message) : base(message)
        {
        }

        public AggregateNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AggregateNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }


    // [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    internal class EventSourcingDbContext : BaseDbContext
    {
        public EventSourcingDbContext() : base("EventSourcing")
        {
        }

        public DbSet<EventSourcing> EventSoucings { get; set; }
    }

    [Table("EventSourcing")]
    internal class EventSourcing
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        [Key]
        [Column(Order = 1)]
        public int Version { get; set; }

        [StringLength(512)]
        public string AggregateType { get; set; }
 
        [StringLength(512)]
        public string EventType { get; set; }

        public string EventData { get; set; }
        
        public DateTime CreatedDate { get; set; }
    }
}
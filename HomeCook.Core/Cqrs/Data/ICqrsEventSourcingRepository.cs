﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace HomeCook.Core.Cqrs.Data
{
    public interface ICqrsEventSourcingRepository<TAggregate> where TAggregate:AggregateRoot
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TAggregate Get(Guid id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregate"></param>
        /// <param name="expectedVersion">-1: automatic get lastest version</param>
        void Save(TAggregate aggregate, int expectedVersion=-1);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregate"></param>
        void CreateNew(TAggregate aggregate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
         /// <param name="aggregateActionsApplyBeforeSave"></param>
        /// <param name="expectedVersion">-1: automatic get lastest version</param>
       void Save(Guid id, Action<TAggregate> aggregateActionsApplyBeforeSave, int expectedVersion=-1);
    }

}

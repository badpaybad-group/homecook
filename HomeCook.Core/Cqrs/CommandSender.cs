﻿using System;
using System.Threading.Tasks;
using HomeCook.Core.Distributed;

namespace HomeCook.Core.Cqrs
{
    public static class CommandSender
    {
        private static readonly int _timeOutInSeconds = 30;

        public static void Send<T>(T cmd) where T : class, ICommand
        {
            DistributedServices.Publish(new DistributedCommand(cmd));
        }

        public static void Send<T>(T cmd, Action<CommandResultNotificationMessage> cmdResultCallBack)
            where T : class, ICommand
        {
            CqrsNotificationServices.Subscribe(cmd.ClientId, cmdResultCallBack);

            DistributedServices.Publish(new DistributedCommand(cmd));
        }

        public static async Task<CommandResultNotificationMessage> SendAsync<T>(T cmd) where T : class, ICommand
        {
            CommandResultNotificationMessage result = null;

            CqrsNotificationServices.Subscribe(cmd.ClientId, cmdResult => result= cmdResult);

            DistributedServices.Publish(new DistributedCommand(cmd));

            var counter = 0;
            while (result==null)
            {
                counter++;
                if(counter>_timeOutInSeconds) throw new TimeoutException($"Timeout process from subcribe channel {cmd.ClientId}");
                await Task.Delay(1000);
            }

            return result;
        }
    }
}
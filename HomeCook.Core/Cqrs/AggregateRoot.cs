using System;
using System.Collections.Generic;

namespace HomeCook.Core.Cqrs
{
    public abstract class AggregateRoot
    {
      
        public Guid Id { get; set; }

        private readonly IList<BaseEvent> _changes = new List<BaseEvent>();

        public IList<BaseEvent> Changes
        {
            get { return _changes; }
        }

        protected void ApplyChange(BaseEvent @event)
        {
            ((dynamic)this).Apply((dynamic)@event);
            _changes.Add(@event);
        }

        public void LoadFromHistory(IList<BaseEvent> eventsHistory)
        {
            foreach (var @event in eventsHistory)
            {
                ((dynamic) this).Apply((dynamic) @event);
            }
        }
    }
}
namespace HomeCook.Core.Cqrs
{
    public interface IEventHandler<T> : ICqrsEventHandler where T : class, IEvent
    {
        void Handle(T evt);
    }

    public interface IProcessOrWorkfollowHandler<T> : ICqrsProcessOrWorkfollowManager
        where T:class,IEvent
    {
        void Handle(T evt);
    }
}
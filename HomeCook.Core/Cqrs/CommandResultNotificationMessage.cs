using HomeCook.Core.Redis;

namespace HomeCook.Core.Cqrs
{
    public class CommandResultNotificationMessage : INotificationMessage
    {
        public CommandResultNotificationMessage()
        {
            
        }
        public CommandResultNotificationMessage(ICommand cmd)
        {
            CommandTypeFullName = cmd.GetType().AssemblyQualifiedName;
            CommandValue = cmd;
            CmdClientId = cmd.ClientId;
        }

        public string CommandTypeFullName { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string CmdClientId { get; set; }
        public object CommandValue { get; set; }
    }
}
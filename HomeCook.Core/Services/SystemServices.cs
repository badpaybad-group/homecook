﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Config;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;
using HomeCook.Core.Mvc;
using HomeCook.Core.Utility;

namespace HomeCook.Core.Services
{
  public  class SystemServices
    {
        ICommandRepository<User> _userRepository=new MySqlCommandRepository<User>();
        IQueryFacade<User> _userQueryFacade=new MySqlQueryFacade<User>();

        ICommandRepository<Role> _roleRepository=new MySqlCommandRepository<Role>();
        IQueryFacade<Role> _roleQueryFacade=new MySqlQueryFacade<Role>();

        ICommandRepository<Right> _rightRepository=new MySqlCommandRepository<Right>();
        IQueryFacade<Right> _rightQueryFacade=new MySqlQueryFacade<Right>();

        public void Init(Assembly currentAssembly)
        {
            InitRole();
            InitRight(currentAssembly);
            InitSupperAdmin();
        }

        private void InitSupperAdmin()
        {
            var existed = _userQueryFacade.Table.FirstOrDefault(i => i.Username.Equals(SystemConfig.AdminUsername,StringComparison.OrdinalIgnoreCase));
            if (existed != null) return;

            _userRepository.Insert(new User()
            {
                Id = Guid.NewGuid(),
                Username = SystemConfig.AdminUsername,
                Password = SystemConfig.AdminPassword.ToPassword(),
                Email="badpaybad@gmail.com",
                Mobile = "01228384839"
            });


        }

        private void InitRight(Assembly currentAssembly)
        {
            var controllers = currentAssembly.GetExportedTypes()
                .Where(type => typeof(IRightMappingController).IsAssignableFrom(type));

            var list =
                controllers.SelectMany(
                        type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(
                        m =>
                            !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute),
                                true).Any())
                    .Select(x => new
                    {
                        Controller = x.DeclaringType.Name.Replace("Controller", string.Empty),
                        Action = x.Name,
                        Attributes = x.GetCustomAttributes().ToList(),
                    })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

            foreach (var l in list)
            {
                var rightName = "/" + l.Controller + "/" + l.Action+"/";
                var existed = _rightQueryFacade.Table.FirstOrDefault(i => i.Name.ToLower() == rightName.ToLower());
                var attrDes =
                    l.Attributes.Where(i => i.GetType() == typeof(RightDescriptionAttribute)).FirstOrDefault() as
                        RightDescriptionAttribute;
                var des = string.Empty;
                if (attrDes != null)
                {
                    des = attrDes.Description;
                }

                if (existed == null)
                {
                    _rightRepository.Insert(new Right()
                    {
                        Id= Guid.NewGuid(),
                        CreatedOn = DateTime.Now,
                        Name = rightName,
                        UpdatedOn = DateTime.Now,
                        Description = des
                    });
                }
                else
                {
                    if (!string.IsNullOrEmpty(des))
                    {
                        existed.Description = des;
                        _rightRepository.Update(existed);
                    }
                }
            }
        }

        private void InitRole()
        {
            var roles = Enum.GetNames(typeof(Role.SystemRole));

            foreach (var role in roles)
            {
                var existed = _roleQueryFacade.Table.FirstOrDefault(i => i.Name.ToLower() == role.ToLower());
                if (existed == null)
                {
                    _roleRepository.Insert(new Role()
                    {
                        Id= Guid.NewGuid(),
                        CreatedOn = DateTime.Now,
                        Name = role,
                        UpdatedOn = DateTime.Now
                    });
                }
            }
        }
    }
}

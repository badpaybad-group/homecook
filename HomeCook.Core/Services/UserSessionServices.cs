﻿using System;
using HomeCook.Core.Config;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Distributed;
using HomeCook.Core.Redis;
using HomeCook.Core.Utility;

namespace HomeCook.Core.Services
{
    public class UserSessionServices
    {
        private readonly IQueryFacade<User> _userQueryFacade;
      
        public UserSessionServices(IQueryFacade<User> userQueryFacade )
        {
            _userQueryFacade = userQueryFacade;
            
        }

        public  bool DoLogin(string uid, string pwd,out string sessionId, out User user)
        {
            sessionId = string.Empty;
            user = null;

            var u = _userQueryFacade.Select(i => i.Username == uid);
           
            if (u==null|| u.Password != pwd.ToPassword())
            {
                return false;
            }

            sessionId = Guid.NewGuid().ToString();
            user = u;
            var expireAfter = new TimeSpan(1, 0, 0);
            SessionServices.Set("UserSessionServices", sessionId, u, expireAfter);

            var loginSuccess = new UserLoginSuccess(sessionId,u,expireAfter);
            var distributedCommand = new DistributedCommand(loginSuccess,DataBehavior.Broadcast);
            DistributedServices.Publish(distributedCommand);

            return true;
        }

        public void DoLogout(string sessionId)
        {
            var u = SessionServices.Get<User>("UserSessionServices", sessionId);

            SessionServices.Remove("UserSessionServices", sessionId);
            
            var logoutSuccess=new UserLogoutSuccess(sessionId, u);

            var distributedCommand = new DistributedCommand(logoutSuccess, DataBehavior.Broadcast);
            DistributedServices.Publish(distributedCommand);
        }

        public bool IsAlive(string sessionId)
        {
            return SessionServices.IsAlive("UserSessionServices", sessionId);
        }

        public User Get(string sessionId)
        {
            var u = SessionServices.Get<User>("UserSessionServices", sessionId);
            return u;
        }
    }

    public class UserLogoutSuccess
    {
        public UserLogoutSuccess(string sessionId, User user)
        {
            SessionId = sessionId;
            User = user;
        }

        public string SessionId { get; }
        public User User { get; }
    }

    public class UserLoginSuccess
    {
        public UserLoginSuccess(string sessionId, User user, TimeSpan expireAfter)
        {
            SessionId = sessionId;
            User = user;
            ExpireAfter = expireAfter;
        }

        public string SessionId { get; }
        public User User { get; }
        public TimeSpan ExpireAfter { get; }
    }

    public static class UserExtension
    {
        public static bool IsAdmin(this User src)
        {
            if (src == null) return false;
            return src.Username.Equals(SystemConfig.AdminUsername, StringComparison.OrdinalIgnoreCase);
        }
    }
}

using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace HomeCook.Core.Data
{
    public class DatabaseInitializer<TContext> : CreateDatabaseIfNotExists<TContext>
        where TContext : DbContext
    {
        public void InitializeDatabase(TContext context)
        {
            //var databaseExists = context.Database.Exists();
            //if (!databaseExists)
            //{
            //    //context.Database.CreateIfNotExists();
            //    // throw new Exception("Should create database name first. Check your connection string in web.config");
            //    var dbCreationScript = ((IObjectContextAdapter)context).ObjectContext.CreateDatabaseScript();
            //    context.Database.ExecuteSqlCommand(dbCreationScript);

            //    context.SaveChanges();
            //}
            base.InitializeDatabase(context);
        }
    }
}
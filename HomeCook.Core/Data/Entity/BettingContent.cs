using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("BettingContent")]
    public class BettingContent : BaseEntity
    {
        public enum ContentType
        {
            None,
            Text,
            Gif,
            Png,
            Video,
            LiveStream
        }

        [Key]
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public Guid CategoryId { get; set; }
        public string ContentUrl { get; set; }
        public int Type { get; set; }

        [NotMapped]
        [Editable(false)]   
        public string TypeText
        {
            get { return ((ContentType) Type).ToString(); }
        }
        public string Description { get; set; }
        public string Title { get; set; }

        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}
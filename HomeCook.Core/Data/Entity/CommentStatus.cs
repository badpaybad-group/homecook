﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("CommentStatus")]
    public class CommentStatus : BaseEntity
    {
      
        [Key]
        public Guid Id { get; set; }
        
        public Guid CommentId { get; set; }

        public Guid UserId { get; set; }

        public bool IsRead { get; set; }
    }
}
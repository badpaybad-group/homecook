﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeCook.Core.Data.Entity
{
    [Table("EntityContentLanguage")]
  public  class EntityContentLanguage:BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid EntityId { get; set; }
        [StringLength(128)]
        public string EntityName { get; set; }
        [StringLength(128)]
        public string FieldName { get; set; }

        public string FieldContent { get; set; }

        public int DisplayOrder { get; set; }
    }
}

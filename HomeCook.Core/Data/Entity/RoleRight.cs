﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("RoleRight")]
    public class RoleRight
    {
        [Key]
        [Column(Order=0)]
        public Guid RoleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid RightId { get; set; }
        
    }
}
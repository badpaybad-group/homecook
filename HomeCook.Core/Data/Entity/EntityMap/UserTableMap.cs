﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeCook.Core.Data.Entity.EntityMap
{
    public class RightTableMap : AbstractTableMapConfiguration<Right>
    {
    }
    public class RoleTableMap : AbstractTableMapConfiguration<Role>
    {
    }
    public class UserVerifyTableMap : AbstractTableMapConfiguration<UserVerify>
    {
    }
    public class UserTableMap : AbstractTableMapConfiguration<User>
    {
    }

    public class BettingContentTableMap : AbstractTableMapConfiguration<BettingContent>
    {
        
    }
    public class BettingTableMap : AbstractTableMapConfiguration<Betting>
    {
        
    }
    public class BettingItemTableMap : AbstractTableMapConfiguration<BettingItem>
    {
        
    }
    public class BettingTransactionTableMap : AbstractTableMapConfiguration<BettingTransaction>
    {
        
    }
    public class CategoryTableMap : AbstractTableMapConfiguration<Category>
    {
        
    }
    public class CommentTableMap : AbstractTableMapConfiguration<Comment>
    {
        
    }
    
    public class CommentStatusTableMap : AbstractTableMapConfiguration<CommentStatus>
    {

    }
}
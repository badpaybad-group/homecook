using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("FileInfo")]
    public class FileInfo : BaseEntity
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [StringLength(256)]
        public string Domain { get; set; }

        [StringLength(2000)]
        public string RelativePath { get; set; }
       public Guid UserId { get; set; }
        public Guid TaskId { get; set; }
        public Guid CommentId { get; set; }
       public Guid ParentId { get; set; }
        public long FileSize { get; set; }
        public string FileType { get; set; }

        [StringLength(256)]
        public string FileName { get; set; }
        [StringLength(2000)]
        public string FileDescription { get; set; }
        [StringLength(128)]
        public string Qquuid { get; set; }
        [StringLength(2000)]
        public string PhysicalPath { get; set; }
    }
}
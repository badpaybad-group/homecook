﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("Role")]
    public class Role:BaseEntity
    {
        public enum SystemRole
        {
            Admin,
        }

        [Key]
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        [StringLength(2000)]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
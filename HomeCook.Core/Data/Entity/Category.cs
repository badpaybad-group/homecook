﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("Category")]
    public class Category:BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string UrlIcon { get; set; }
        public string Name { get; set; }
    }
}
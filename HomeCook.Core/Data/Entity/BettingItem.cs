using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("BettingItem")]
    public class BettingItem : BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public Guid BettingId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        
        public string StyleCss { get; set; }
        public string ImageUrl { get; set; }
        public decimal OpeningOdds { get; set; }

        public bool IsHappened { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("Right")]
    public class Right:BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }
    
        public string Description { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("BettingTransaction")]
    public class BettingTransaction : BaseEntity
    {
        public enum BettingTransactionStatus
        {
            None,
            Accepted,
            Rejected,
            Cancel
        }

        public enum BettingTransactionResultStatus
        {
            None,
            Win,
            Lose,
            Refunded,
            Void,
            Cancel
        }
        [Key]
        public Guid Id { get; set; }
        public Guid ContentId { get; set; }
        public Guid BettingId { get; set; }
        public Guid BettingItemId { get; set; }
        public Guid UserId { get; set; }
        public decimal Odds { get; set; }
        public decimal AcceptedAmount { get; set; }
        public int Status { get; set; }

        [NotMapped]
        [Editable(false)]
        public string StatusText { get { return ((BettingTransactionStatus)Status).ToString(); } }
        public DateTime BettingProcessedAt { get; set; }

        public int ResultStatus { get; set; }


        
    }
}
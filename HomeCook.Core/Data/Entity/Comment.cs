﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("Comment")]
    public class Comment : BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        
        public Guid ParentId { get; set; }
        
        public Guid SourceId { get; set; }
        
        public Guid FromUserId { get; set; }
        
        public Guid ToUserId { get; set; }

        public string Message { get; set; }
    }
}
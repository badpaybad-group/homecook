﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("Betting")]
    public class Betting : BaseEntity
    {
        public enum BettingStatus
        {
            None,
            Starting,
            Started,
            Stopping,
            Stopped,
            Declared,
            Cancel
        }

        [Key]
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public Guid BettingContentId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public int Status { get; set; }

        public int MinChoice { get; set; }
        public int MaxChoice { get; set; }

        [NotMapped]
        [Editable(false)]
        public string StatusText {get { return ((BettingStatus) Status).ToString(); } }
    }
}
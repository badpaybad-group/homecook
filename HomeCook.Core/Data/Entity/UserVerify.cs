﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("UserVerify")]
    public class UserVerify:BaseEntity
    {
        public enum UserVerifyType
        {
            Username,
            Email,
            Phone
        }

        [Key]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string VerifyValue { get; set; }
        public string VerifyCode { get; set; }
        public bool Verify { get; set; }
        public int Type { get; set; }
        public int ExpiredInSecond { get; set; }
    }
}
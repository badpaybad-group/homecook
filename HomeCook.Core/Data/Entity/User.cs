﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace HomeCook.Core.Data.Entity
{
    [Table("User")]
    public class User:BaseEntity
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTime LastLoginOn { get; set; }
        
        //[Editable(true)]
        [StringLength(128)]
        public string Password { get; set; }

        [StringLength(256)]
        //[Index(IsUnique=true)]
        public string Username { get; set; }
        public string Fullname { get; set; }
        
        [StringLength(256)]
        //[Index(IsUnique = true)]
        public string Email { get; set; }

        [StringLength(256)]
        //[Index(IsUnique = true)]
        public string Mobile { get; set; }

        public Guid ParentId { get; set; }
        public bool Actived { get; set; }

        [Editable(false)]
        [NotMapped]
        public string ActiveText {get { return Actived ? "Đã kích hoạt" : "Chưa kích hoạt"; } }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    [Table("UserRole")]
    public class UserRole : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        public Guid UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid RoleId { get; set; }
    }
}
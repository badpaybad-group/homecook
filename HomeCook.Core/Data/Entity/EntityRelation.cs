﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeCook.Core.Data.Entity
{
    public class EntityRelation : BaseEntity
    {

        [Key]
        public Guid Id { get; set; }
        public Guid EntityId { get; set; }
      
        [StringLength(128)]
        public string EntityName { get; set; }
     
        public Guid RelationEntityId { get; set; }
      
        [StringLength(128)]
        public string RelationEntityName { get; set; }

        public int DisplayOrder { get; set; }
    }
}
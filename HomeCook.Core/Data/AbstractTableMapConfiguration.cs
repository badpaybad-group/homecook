using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace HomeCook.Core.Data
{
    public abstract class AbstractTableMapConfiguration<T> : EntityTypeConfiguration<T> where T : class
    {
     
        protected AbstractTableMapConfiguration()
        {
            var tableName = GetTableName<T>();
           
            ToTable(tableName);
        }

        public string GetTableName<T>()
        {
            var dnAttribute = typeof(T).GetCustomAttributes(typeof(TableAttribute), true
            ).FirstOrDefault() as TableAttribute;

            string tableName=string.Empty;

            if (dnAttribute != null)
            {
                tableName= dnAttribute.Name;
            }

            if (string.IsNullOrEmpty(tableName))
                tableName = typeof(T).Name;

            return tableName;
        }
    }

}
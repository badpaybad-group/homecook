using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Utility;

namespace HomeCook.Core.Data.MySql
{
    public class MySqlCommandRepository<TEntity> : ICommandRepository<TEntity> where TEntity : BaseEntity
    {
        public virtual void InsertOrUpdate(params TEntity[] entities)
        {
            using (var db = new HomeCookDbContext())
            {
                var dbSet = db.Set<TEntity>();
                dbSet.AddOrUpdate(entities);
                db.SaveChanges();
            }

        }

        public virtual void Insert(params TEntity[] entities)
        {
            using (var db = new HomeCookDbContext())
            {
                var dbSet = db.Set<TEntity>();
                dbSet.AddRange(entities);
                db.SaveChanges();
            }

        }

        public virtual void Update(params TEntity[] entities)
        {
            var notMappeds = GetIgnoreOnUpdateProperties();

            var propsEntity = typeof(TEntity).GetProperties();

            using (var db = new HomeCookDbContext())
            {
                foreach (var entity in entities)
                {
                    var dbEntityEntry = db.Entry(entity);

                    var entry = dbEntityEntry;

                    if (entry.State == EntityState.Detached)
                    {
                        var dbSet = db.Set<TEntity>();
                        dbSet.Attach(entity);
                    }
                    if (notMappeds == null || notMappeds.Count == 0)
                    {
                        entry.State = EntityState.Modified;
                    }
                    else
                    {
                        foreach (var p in propsEntity)
                        {
                            if (!p.PropertyType.IsPrimitiveOrComplex()) continue;

                            if (!notMappeds.TryGetValue(p.Name, out bool isNotmapped))
                            {
                                entry.Property(p.Name).IsModified = true;
                            }
                        }
                    }
                }

                db.SaveChanges();
            }

        }

        public void UpdateExcludeProperty(List<string> excludeNames, params TEntity[] entities)
        {
           
            var propsEntity = typeof(TEntity).GetProperties();
           

            using (var db = new HomeCookDbContext())
            {
                foreach (var entity in entities)
                {
                    var dbEntityEntry = db.Entry(entity);

                    var entry = dbEntityEntry;

                    if (entry.State == EntityState.Detached)
                    {
                        var dbSet = db.Set<TEntity>();
                        dbSet.Attach(entity);
                    }

                    if (excludeNames != null)
                    {
                        foreach (var p in propsEntity)
                        {
                            if (!p.PropertyType.IsPrimitiveOrComplex()) continue;

                            if (excludeNames.Any(n => n.Equals(p.Name, StringComparison.OrdinalIgnoreCase)))
                            {
                                entry.Property(p.Name).IsModified = false;
                            }
                            else
                            {
                                entry.Property(p.Name).IsModified = true;
                            }
                        }
                    }
                    else
                    {
                        entry.State = EntityState.Modified;
                    }
                }

                db.SaveChanges();
            }
        }

        static Dictionary<string, bool> GetIgnoreOnUpdateProperties()
        {
            var notMapProps = new Dictionary<string, bool>();

            var props = typeof(TEntity).GetProperties();
            foreach (var p in props)
            {
                var isReadOnly = p.SetMethod == null || !p.SetMethod.IsPublic;
                if (isReadOnly)
                {
                    notMapProps.Add(p.Name, false);
                    continue;
                }
                var attrs = p.GetCustomAttributes(true);
                foreach (var a in attrs)
                {
                    if (notMapProps.ContainsKey(p.Name)) continue;

                    var nma = a as NotMappedAttribute;
                    if (nma != null)
                    {
                        notMapProps.Add(p.Name, false);
                    }
                }
            }

            return notMapProps;
        }

        public void UpdateIncludeProperty(List<string> includeNames, params TEntity[] entities)
        {
            var propsEntity = typeof(TEntity).GetProperties();
          

            using (var db = new HomeCookDbContext())
            {
                foreach (var entity in entities)
                {
                    var dbEntityEntry = db.Entry(entity);

                    var entry = dbEntityEntry;

                    if (entry.State == EntityState.Detached)
                    {
                        var dbSet = db.Set<TEntity>();
                        dbSet.Attach(entity);
                    }

                    if (includeNames != null)
                    {
                        foreach (var p in propsEntity)
                        {
                            if (!p.PropertyType.IsPrimitiveOrComplex()) continue;

                            if (includeNames.Any(n => n.Equals(p.Name, StringComparison.OrdinalIgnoreCase)))
                            {
                                entry.Property(p.Name).IsModified = true;
                            }
                            else
                            {
                                entry.Property(p.Name).IsModified = false;
                            }
                        }
                    }
                    else
                    {
                        entry.State = EntityState.Modified;
                    }
                }

                db.SaveChanges();
            }
        }

        public virtual void Delete(params TEntity[] entities)
        {
            using (var db = new HomeCookDbContext())
            {
                foreach (var entity in entities)
                {
                    var dbEntityEntry = db.Entry(entity);

                    var entry = dbEntityEntry;
                    var dbSet = db.Set<TEntity>();

                    if (entry.State == EntityState.Detached)
                    {
                        dbSet.Attach(entity);
                    }

                    dbSet.Remove(entity);
                }

                db.SaveChanges();
            }

        }

        public virtual void Delete(params object[] ids)
        {
            using (var db = new HomeCookDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var entities = new List<TEntity>();
                foreach (var id in ids)
                {
                    var entity = dbSet.Find(id);
                    if (entity != null)
                    {
                        entities.Add(entity);
                    }
                }
                dbSet.RemoveRange(entities);
                db.SaveChanges();
            }

        }

        public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            using (var db = new HomeCookDbContext())
            {
                var dbSet = db.Set<TEntity>();
                dbSet.RemoveRange(dbSet.Where(predicate));
                db.SaveChanges();
            }

        }

       
    }
}
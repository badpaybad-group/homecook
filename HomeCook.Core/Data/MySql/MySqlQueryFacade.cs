﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Utility;

namespace HomeCook.Core.Data.MySql
{
    public class MySqlQueryFacade<TEntity> : IQueryFacade<TEntity> where TEntity : BaseEntity
    {
        private readonly HomeCookDbContext _dbContext;

        public MySqlQueryFacade()
        {
            _dbContext = new HomeCookDbContext();
        }

        public virtual TEntity Select(params object[] ids)
        {
            //return Table.FirstOrDefault(i => i.Id == id);
            return _dbContext.Set<TEntity>().Find(ids);
        }

        public TEntity Select(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().AsNoTracking().SingleOrDefault(predicate);
        }

        public List<TEntity> SelectBy(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().AsNoTracking().Where(predicate).ToList();
        }

        public List<TEntity> SelectBy(Expression<Func<TEntity, bool>> predicate, string sortField, string sortBy
            , int skip, int take, out int total)
        {
            var queryable = _dbContext.Set<TEntity>().AsNoTracking().Where(predicate);
            total = queryable.Count();
            if (sortBy.Equals("desc", StringComparison.OrdinalIgnoreCase))
            {
                return queryable
                    .OrderByDescendingFieldName(sortField)
                    .Skip(skip).Take(take).ToList();
            }
            else
            {
                return queryable
                    .OrderByFieldName(sortField)
                    .Skip(skip).Take(take).ToList();
            }
        }


        public IEnumerable<TEntity> Table
        {
            get { return _dbContext.Set<TEntity>().AsNoTracking(); }
        }

        //public virtual void InsertOrUpdate(params TEntity[] entities)
        //{
        //    using (var db=new HomeCookDbContext())
        //    {
        //        var dbSet = db.Set<TEntity>();
        //        dbSet.AddOrUpdate(entities);
        //        db.SaveChanges();
        //    }
          
        //}

        //public virtual void Insert(params TEntity[] entities)
        //{
        //    using (var db = new HomeCookDbContext())
        //    {
        //        var dbSet = db.Set<TEntity>();
        //        dbSet.AddRange(entities);
        //        db.SaveChanges();
        //    }
              
        //}

        //public virtual void Update(params TEntity[] entities)
        //{
        //    using (var db = new HomeCookDbContext())
        //    {

        //        foreach (var entity in entities)
        //        {
        //            var dbEntityEntry = db.Entry(entity);

        //            var entry = dbEntityEntry;

        //            if (entry.State == EntityState.Detached)
        //            {
        //                var dbSet = db.Set<TEntity>();
        //                dbSet.Attach(entity);
        //            }

        //            entry.State = EntityState.Modified;
        //        }

        //        db.SaveChanges();
        //    }
          
        //}

        //public void Update<TChange>(params TEntity[] entities)
        //{
        //    var propsToChange = typeof(TChange).GetProperties();
        //    var propsEntity = typeof(TEntity).GetProperties();

        //    using (var db = new HomeCookDbContext())
        //    {
               

        //        foreach (var entity in entities)
        //        {
        //            var dbEntityEntry = db.Entry(entity);

        //            var entry = dbEntityEntry;

        //            if (entry.State == EntityState.Detached)
        //            {
        //                var dbSet = db.Set<TEntity>();
        //                dbSet.Attach(entity);
        //            }

        //            foreach (var temp in propsToChange)
        //            {
        //                if (!temp.PropertyType.IsPrimitiveOrComplex()) continue;

        //                if (propsEntity.Any(e => e.Name.Equals(temp.Name, StringComparison.OrdinalIgnoreCase)))
        //                {
        //                    entry.Property(temp.Name).IsModified = true;
        //                }
        //                else
        //                {
        //                    entry.Property(temp.Name).IsModified = false;
        //                }
        //            }
        //        }

        //        db.SaveChanges();
        //    }
         
        //}

        //public virtual void Delete(params TEntity[] entities)
        //{
        //    using (var db = new HomeCookDbContext())
        //    {
        //        foreach (var entity in entities)
        //        {
        //            var dbEntityEntry = db.Entry(entity);

        //            var entry = dbEntityEntry;
        //            var dbSet = db.Set<TEntity>();

        //            if (entry.State == EntityState.Detached)
        //            {
        //                dbSet.Attach(entity);
        //            }

        //            dbSet.Remove(entity);
        //        }

        //        db.SaveChanges();
        //    }
          
        //}

        //public virtual void Delete(params object[] ids)
        //{
        //    using (var db = new HomeCookDbContext())
        //    {
        //        var dbSet = db.Set<TEntity>();
        //        var entities = new List<TEntity>();
        //        foreach (var id in ids)
        //        {
        //            var entity = dbSet.Find(id);
        //            if (entity != null)
        //            {
        //                entities.Add(entity);
        //            }
        //        }
        //        dbSet.RemoveRange(entities);
        //        db.SaveChanges();
        //    }
          
        //}

        //public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
        //{
        //    using (var db = new HomeCookDbContext())
        //    {
        //        var dbSet = db.Set<TEntity>();
        //        dbSet.RemoveRange(dbSet.Where(predicate));
        //        db.SaveChanges();
        //    }
         
        //}

    }
}

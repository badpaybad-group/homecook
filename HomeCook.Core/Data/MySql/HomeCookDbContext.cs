using System.Data.Common;
using System.Data.Entity;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Core.Data.MySql
{
  
    public class HomeCookDbContext : BaseDbContext
    {
        public HomeCookDbContext() : base("HomeCook")
        {
        
        }
        public HomeCookDbContext(string nameConnectionString) : base(nameConnectionString)
        {

        }
        public HomeCookDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {

        }
     

    }
}
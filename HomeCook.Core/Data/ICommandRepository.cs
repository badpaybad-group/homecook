﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Core.Data
{
   public interface ICommandRepository<TEntity> where TEntity : BaseEntity
    {
        void InsertOrUpdate(params TEntity[] entities);
        void Insert(params TEntity[] entities);
        void Update(params TEntity[] entities);
        //void Update<TChange>(params TEntity[] entities);
        void UpdateExcludeProperty(List<string> excludeNames, params TEntity[] entities);
        void UpdateIncludeProperty(List<string> includeNames, params TEntity[] entities);
        void Delete(params TEntity[] entities);
        void Delete(params object[] ids);
        void Delete(Expression<Func<TEntity, bool>> predicate);
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HomeCook.Core.Data
{
    public interface IQueryFacade<TEntity>
    {
  
        TEntity Select(params object[] ids);
        TEntity Select(Expression<Func<TEntity, bool>> predicate);
        List<TEntity> SelectBy(Expression<Func<TEntity, bool>> predicate);
        List<TEntity> SelectBy(Expression<Func<TEntity, bool>> predicate, string sortField, string sortBy, int skip, int take, out int total);
        
        IEnumerable<TEntity> Table { get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace HomeCook.Core.Utility
{
    public class ImageResizer
    {
        public static readonly Dictionary<string, ImageFormat> FileExtensionSupport = new Dictionary<string, ImageFormat>();
        public static readonly Dictionary<ImageFormat, string> ContentType=new Dictionary<ImageFormat, string>(); 
        static ImageResizer()
        {
            FileExtensionSupport["jpg"] = ImageFormat.Jpeg;
            FileExtensionSupport[".jpg"] = ImageFormat.Jpeg;
            FileExtensionSupport["png"] = ImageFormat.Png;
            FileExtensionSupport[".png"] = ImageFormat.Png;
            FileExtensionSupport["jpeg"] = ImageFormat.Jpeg;
            FileExtensionSupport[".jpeg"] = ImageFormat.Jpeg;
            FileExtensionSupport["gif"] = ImageFormat.Gif;
            FileExtensionSupport[".gif"] = ImageFormat.Gif;
            FileExtensionSupport["bmp"] = ImageFormat.Bmp;
            FileExtensionSupport[".bmp"] = ImageFormat.Bmp;

            ContentType[ImageFormat.Jpeg] = GetMimeType(ImageFormat.Jpeg);
            ContentType[ImageFormat.Png] = GetMimeType(ImageFormat.Png);
            ContentType[ImageFormat.Gif] = GetMimeType(ImageFormat.Gif);
            ContentType[ImageFormat.Bmp] = GetMimeType(ImageFormat.Bmp);
        }
        public static string GetMimeType( ImageFormat imageFormat)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            return codecs.First(codec => codec.FormatID == imageFormat.Guid).MimeType;
        }
        public static bool IsSupport(string fileExtension, out ImageFormat imgFormat)
        {
            fileExtension = fileExtension.ToLower();

            return FileExtensionSupport.TryGetValue(fileExtension, out imgFormat);
        }

        public static MemoryStream ResizeImageFileIntoMemoryStream(System.Drawing.Image oldImage, Size targetSize,
            ResizeMode rMode, string fileExtension)
        {
            fileExtension = fileExtension.ToLower();
            ImageFormat imgType;
            if (!FileExtensionSupport.TryGetValue(fileExtension, out imgType))
            {
                throw new Exception("Not support file extension: "+ fileExtension);
            }

            MemoryStream m = new MemoryStream();
            Size newSize = CalculateDimensions(oldImage.Size, targetSize, rMode);
            using (Bitmap newImage = new Bitmap(newSize.Width, newSize.Height, PixelFormat.Format32bppRgb))
            {
                using (Graphics canvas = Graphics.FromImage(newImage))
                {
                    canvas.SmoothingMode = SmoothingMode.Default;
                    canvas.InterpolationMode = InterpolationMode.Default;
                    canvas.PixelOffsetMode = PixelOffsetMode.Default;
                    canvas.DrawImage(oldImage, new Rectangle(new Point(0, 0), newSize));
                    newImage.Save(m, imgType);
                }
            }
            return m;
        }

        private static Size CalculateDimensions(Size oldSize, Size newSize, ResizeMode rMode)
        {
            if (newSize.Width > oldSize.Width || newSize.Height> oldSize.Height)
            {
                return oldSize;
            }
            int targetSize = newSize.Width;
            switch (rMode)
            {
                case ResizeMode.OnlyHeight:
                    targetSize = newSize.Height;
                    newSize.Width = (int) (oldSize.Width*((float) targetSize/(float) oldSize.Height));
                    newSize.Height = targetSize;
                    break;
                case ResizeMode.OnlyWidth:
                    targetSize = newSize.Width;
                    newSize.Width = targetSize;
                    newSize.Height = (int) (oldSize.Height*((float) targetSize/(float) oldSize.Width));
                    break;
                case ResizeMode.WidthAndHeight:

                    break;
                default:
                    targetSize = newSize.Width;
                    if (oldSize.Height > oldSize.Width)
                    {
                        newSize.Width = (int) (oldSize.Width*((float) targetSize/(float) oldSize.Height));
                        newSize.Height = targetSize;
                    }
                    else
                    {
                        newSize.Width = targetSize;
                        newSize.Height = (int) (oldSize.Height*((float) targetSize/(float) oldSize.Width));
                    }
                    break;
            }
            return newSize;
        }

        public enum ResizeMode
        {
            Auto = 0,
            OnlyWidth = 1,
            OnlyHeight = 2,
            WidthAndHeight = 3
        }
    }
}
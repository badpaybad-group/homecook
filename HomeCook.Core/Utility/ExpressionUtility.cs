﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HomeCook.Core.Utility
{
    public static class ExpressionUtility
    {
        public static Expression<Func<TSource, object>> GetExpression<TSource>(string propertyName)
        {
            var param = Expression.Parameter(typeof(TSource), "x");
            Expression conversion = Expression.Convert(Expression.Property
            (param, propertyName), typeof(object));   //important to use the Expression.Convert
            return Expression.Lambda<Func<TSource, object>>(conversion, param);
        }

        //makes deleget for specific prop
        public static Func<TSource, object> GetFunc<TSource>(string propertyName)
        {
            return GetExpression<TSource>(propertyName).Compile();  //only need compiled expression
        }

        //OrderBy overload
        public static IOrderedEnumerable<TSource> OrderByFieldName<TSource>(this IEnumerable<TSource> source, string propertyName)
        {
            return source.OrderBy(GetFunc<TSource>(propertyName));
        }

        //OrderBy overload
        //public static IOrderedQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> source, string propertyName)
        //{
        //    return source.OrderBy(GetExpression<TSource>(propertyName));
        //}

        public static IOrderedEnumerable<TSource> OrderByDescendingFieldName<TSource>(this IEnumerable<TSource> source, string propertyName)
        {
            return source.OrderByDescending(GetFunc<TSource>(propertyName));
        }

        //public static IOrderedQueryable<TSource> OrderByDescending<TSource>(this IQueryable<TSource> source, string propertyName)
        //{
        //    return source.OrderByDescending( GetExpression(typeof( TSource) , propertyName));
        //}

        //private static string GetExpression(Type type, string propertyName)
        //{
        //    var param = Expression.Parameter(type), "x");
        //    Expression conversion = Expression.Convert(Expression.Property
        //    (param, propertyName), typeof(object));   //important to use the Expression.Convert
        //    return Expression.Lambda<Func<TSource, object>>(conversion, param);
        //}
    }
}

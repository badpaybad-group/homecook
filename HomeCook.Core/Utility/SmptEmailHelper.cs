using System;
using System.Net;
using System.Net.Mail;

namespace HomeCook.Core.Utility
{
    public class SmptEmailHelper:IDisposable
    {
        private MailAddress mailAddress = null;
        private string _smtpServer = "";
        private int _smtpPort = 587;
        private string _siteEmail = "";
        private string _siteEmailPass = "";
        private string errorMessage = string.Empty;

        public SmptEmailHelper() { }

        public SmptEmailHelper(string smtpServer, int smptPort, string siteEmail, string siteEmailPass)
        {
            mailAddress = new MailAddress(siteEmail, siteEmail);
            _siteEmail = siteEmail;
            _siteEmailPass = siteEmailPass;
            _smtpServer = smtpServer;
            _smtpPort = smptPort;
        }

        public void SetConfig(string smtpServer, int smptPort, string siteEmail, string siteEmailPass)
        {
            mailAddress = new MailAddress(siteEmail, siteEmail);
            _siteEmail = siteEmail;
            _siteEmailPass = siteEmailPass;
            _smtpServer = smtpServer;
            _smtpPort = smptPort;
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            set { errorMessage = value; }
            get { return errorMessage; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="ccEmail"> cc: badpaybad@yahoo.com,badpaybad@gmail.com  </param>
        /// <param name="title"></param>
        /// <param name="body"></param>
        /// <param name="usingHTML"></param>
        public void SendLikeGmail(string toEmail, string ccEmail, string title, string body, bool usingHTML, bool enablessl=false)
        {
            var eFrom = new MailAddress(_siteEmail, _siteEmail);
            var eTo = new MailAddress(toEmail, toEmail);

            var sc = new SmtpClient(_smtpServer, _smtpPort)
            {
                EnableSsl = enablessl,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(_siteEmail, _siteEmailPass),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Timeout = 30 * 1000
            };
            var mm = new MailMessage(eFrom, eTo);
            if (ccEmail.Trim() != "")
            {
                mm.CC.Add(ccEmail);
            }

            mm.Subject = title;
            mm.Body = body;
            mm.IsBodyHtml = usingHTML;
            sc.Send(mm);

        }

        //public void SendMail(string to, string bcc, string cc, string subject, string body)
        //{

        //    // Instantiate a new instance of MailMessage
        //    MailMessage mMailMessage = new MailMessage();
        //    // Set the sender address of the mail message
        //    mMailMessage.From = mailAddress;
        //    // Set the recepient address of the mail message
        //    mMailMessage.To.Add(new MailAddress(to));
        //    // Check if the bcc value is null or an empty string
        //    if (!string.IsNullOrEmpty(bcc))
        //    {
        //        // Set the Bcc address of the mail message
        //        mMailMessage.Bcc.Add(new MailAddress(bcc));
        //    }
        //    // Check if the cc value is null or an empty value
        //    if (!string.IsNullOrEmpty(cc))
        //    {
        //        // Set the CC address of the mail message
        //        mMailMessage.CC.Add(new MailAddress(cc));
        //    } // Set the subject of the mail message
        //    mMailMessage.Subject = subject;
        //    // Set the body of the mail message
        //    mMailMessage.Body = body;
        //    // Set the format of the mail message body as HTML
        //    mMailMessage.IsBodyHtml = true;
        //    // Set the priority of the mail message to normal
        //    mMailMessage.Priority = MailPriority.Normal;
        //    // Instantiate a new instance of SmtpClient
        //    SmtpClient mSmtpClient = new SmtpClient();
        //    // Send the mail message

        //    mSmtpClient.Send(mMailMessage);


        //}

        public void SendMail(string emailTo, string subject, string body)
        {
            SmtpClient smtpClient = new SmtpClient(_smtpServer,_smtpPort);
            smtpClient.Timeout = 30*1000;
            smtpClient.Credentials = new NetworkCredential(mailAddress.Address, mailAddress.Address);
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = mailAddress;
                mailMessage.To.Add(new MailAddress(emailTo));
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.Encoding.Unicode;
                mailMessage.SubjectEncoding = System.Text.Encoding.Unicode;
                mailMessage.Priority = MailPriority.High;

                smtpClient.Send(mailMessage);


            }

        }

        public void Dispose()
        {
            
        }
    }
}
﻿using System;
using System.Linq;

namespace HomeCook.Core.Utility
{
    public static class TypeExtensions
    {
        private static Type[] _complexType = new Type[]
        {
            typeof (String),
            typeof (string),
            typeof (Decimal),
            typeof (decimal),
            typeof (DateTime),
            typeof (DateTimeOffset),
            typeof (TimeSpan),
            typeof (Guid)
        };

        public static bool IsPrimitiveOrComplex(this Type type)
        {
            var typeCode = Convert.GetTypeCode(type);

            var contains = _complexType.Contains(type);

            return
             type.IsValueType ||
             type.IsPrimitive ||
             contains ||
             typeCode != TypeCode.Object;
        }
    }
}

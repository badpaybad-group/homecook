﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;

namespace HomeCook.Core.Mvc
{
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        private readonly Container _iocContainer;
        public StructureMapControllerFactory(Container iocContainer)
        {
            _iocContainer = iocContainer;
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null) return null;
            return _iocContainer.GetInstance(controllerType) as Controller;
        }
    }
}
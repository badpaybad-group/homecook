﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using StructureMap;

namespace HomeCook.Core.Mvc
{
    public class StructureMapApiControllerFactory : IHttpControllerActivator
    {
        private readonly Container _iocContainer;
        private readonly HttpConfiguration _configuration;
        public StructureMapApiControllerFactory(HttpConfiguration configuration, Container iocContainer)
        {
            _configuration = configuration;
            _iocContainer = iocContainer;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {

            var controller = _iocContainer.GetInstance(controllerType) as IHttpController;
            return controller;
        }
    }
}
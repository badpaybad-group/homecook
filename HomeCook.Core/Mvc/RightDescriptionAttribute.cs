﻿using System;

namespace HomeCook.Core.Mvc
{
    [AttributeUsage(AttributeTargets.All, Inherited = true)]
    public class RightDescriptionAttribute : Attribute
    {
        private readonly string _description;
        public string Description
        {
            get { return _description; }
        }

        public RightDescriptionAttribute(string description)
        {
            _description = description;
        }
    }
}
﻿using System;
using System.Configuration;
using StackExchange.Redis;

namespace HomeCook.Core.Redis
{
    public static class RedisHelper
    {
        static IServer _server;
        static SocketManager _socketManager;
        static IConnectionMultiplexer _connectionMultiplexer;

        public static IConnectionMultiplexer RedisConnectionMultiplexer
        {
            get
            {
                if (_connectionMultiplexer != null && _connectionMultiplexer.IsConnected)
                    return _connectionMultiplexer;

                if (_connectionMultiplexer != null && !_connectionMultiplexer.IsConnected)
                {
                    _connectionMultiplexer.Dispose();
                }

                _connectionMultiplexer = GetConnection();
                if (_connectionMultiplexer.IsConnected) return _connectionMultiplexer;

                var exception = new Exception("Can not connect to redis");
                Console.WriteLine(exception);
                throw exception;
            }
        }

        public static IDatabase RedisDatabase
        {
            get
            {
                var redisDatabase = RedisConnectionMultiplexer.GetDatabase();
               
                return redisDatabase;
            }
        }

        public static ISubscriber RedisSubscriber
        {
            get
            {
                var redisSubscriber = RedisConnectionMultiplexer.GetSubscriber();
               
                return redisSubscriber;
            }
        }

        static RedisHelper()
        {
            _socketManager = new SocketManager("JulJulCoreSocketManager");
        }

        public static ConnectionMultiplexer GetConnection()
        {
            var host = ConfigurationManager.AppSettings["RedisHost"]?? "127.0.0.1";
            var port =int.Parse(ConfigurationManager.AppSettings["RedisPort"]??"6379") ;
            var pwd = ConfigurationManager.AppSettings["RedisPwd"] ?? "badpaybad.info";
            var options = new ConfigurationOptions
            {
                EndPoints =
                {
                    {host, port}
                },
                Password = pwd,
                AllowAdmin = false,
                SyncTimeout = 5*1000,
                SocketManager = _socketManager,
                AbortOnConnectFail = false,
                ConnectTimeout = 5*1000,
            };

            return ConnectionMultiplexer.Connect(options);
        }

        public static bool Init()
        {
             return RedisConnectionMultiplexer.IsConnected;
        }
    }


}
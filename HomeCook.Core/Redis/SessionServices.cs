using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace HomeCook.Core.Redis
{
    public static class SessionServices
    {
        const string _sessionServices = "SessionServices";
  
        public static T Get<T>(string clientId, string key)
        {
            var name = clientId+"_"+ key;
            var c = RedisHelper.RedisDatabase.StringGet(name);
            if (!c.HasValue) return default(T);

            return JsonConvert.DeserializeObject<T>(c);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="clientId"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="expireAfter">null will never expire</param>
        public static void Set<T>(string clientId, string key, T val, TimeSpan expireAfter)
        {
            var name = clientId + "_" + key;
            RedisHelper.RedisDatabase.StringSet(name, JsonConvert.SerializeObject(val), expireAfter);

            RedisHelper.RedisDatabase.HashSet(_sessionServices, name, typeof(T).FullName);
        }
       

        public static T Get<T>(string clientId, string key, TimeSpan expireAfter, Func<T> buildDataIfNotExist)
        {
            var name = clientId + "_" + key;

            var c = RedisHelper.RedisDatabase.StringGet(name);
            if (c.HasValue) return JsonConvert.DeserializeObject<T>(c);

            if (buildDataIfNotExist == null) return default(T);

            var r = buildDataIfNotExist();
            RedisHelper.RedisDatabase.StringSet(name, JsonConvert.SerializeObject(r), expireAfter);

            RedisHelper.RedisDatabase.HashSet(_sessionServices, name, typeof(T).FullName);
            return r;
        }

        public static void KeyExpire(string clientId, string key, TimeSpan time)
        {
            var name = clientId + "_" + key;

            var date = DateTime.Now.Add(time);

            RedisHelper.RedisDatabase.KeyExpire(name, date, CommandFlags.FireAndForget);
        }

        public static void Remove(string clientId, string key)
        {
            var name = clientId + "_" + key;
            RedisHelper.RedisDatabase.KeyDelete(name);
            RedisHelper.RedisDatabase.HashDelete(_sessionServices, name);
        }

        public static List<string> GetAllKey()
        {
            return RedisHelper.RedisDatabase.HashGetAll(_sessionServices)
                .Select(i => i.Name.ToString()).ToList();
        }

        public static bool IsAlive(string clientId, string key)
        {
            var name = clientId + "_" + key;

            var c = RedisHelper.RedisDatabase.StringGet(name);
            return c.HasValue;
        }
    }
}
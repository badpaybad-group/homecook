﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Cqrs;
using Newtonsoft.Json;
using StructureMap.Building;

namespace HomeCook.Core.Redis
{
    public interface INotificationMessage
    {
    }

  
    public static class NotificationServices
    {
        static readonly int _timeoOut=30;
        public static void Push<T>(string channel, T msg) where T : INotificationMessage
        {
            var keyChannel = channel;
            keyChannel = keyChannel.ToLower();
            var msgJsonData = JsonConvert.SerializeObject(msg);
            RedisHelper.RedisSubscriber.Publish(keyChannel, msgJsonData);
        }

        public static async Task<TR> PushAsync<T,TR>(string channel, T msg) where T : INotificationMessage 
            where TR:class ,new()
        {
            Push(channel,msg);
            TR result = null;

            Subscribe<TR>(channel,r => result=r );
           
            var counter = 0;
            while (result==null)
            {
                counter++;
                if(counter>_timeoOut) throw new TimeoutException($"Timeout channel {channel}");
                await Task.Delay(1000);
            }

            return result;
        }

        public static void Subscribe<TR>(string channel, Action<TR> callBack)
        {
            var keyChannel = channel;
            keyChannel = keyChannel.ToLower();
        
            RedisHelper.RedisSubscriber.Subscribe(keyChannel, (redisChannel, value) =>
            {
                var msg = JsonConvert.DeserializeObject<TR>(value);
                callBack(msg);
            });
        }

        public static void Unsubscribe(string channel)
        {
            var keyChannel = channel;
            keyChannel = keyChannel.ToLower();
            RedisHelper.RedisSubscriber.Unsubscribe(keyChannel);
        }
    }
}
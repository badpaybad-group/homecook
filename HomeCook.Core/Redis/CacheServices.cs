using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace HomeCook.Core.Redis
{
    public static class CacheServices
    {
        const string _cacheServices = "CacheServices";
        const string _cacheServicesType = "CacheServicesType";

        public static T Get<T>(string key, Func<T> buildDataIfNotExist)
        {
            var val = RedisHelper.RedisDatabase.HashGet(_cacheServices, key);

            var c = JsonConvert.DeserializeObject<T>(val);
            if (c != null) return (T) c;

            var r = buildDataIfNotExist();
            var redisval = JsonConvert.SerializeObject(r);
            RedisHelper.RedisDatabase.HashSet(_cacheServices, key, redisval);
            RedisHelper.RedisDatabase.HashSet(_cacheServicesType, key, typeof(T).FullName);
            return r;
        }

        public static T Get<T>(string key)
        {
            var val = RedisHelper.RedisDatabase.HashGet(_cacheServices, key);
            
            var c = JsonConvert.DeserializeObject<T>(val);

            if (c == null) return default(T);
            return (T) c;
        }

        public static void Set<T>(string key, T val)
        {
            var redisval = JsonConvert.SerializeObject(val);

            RedisHelper.RedisDatabase.HashSet(_cacheServices, key, redisval);
            RedisHelper.RedisDatabase.HashSet(_cacheServicesType, key, typeof(T).FullName);
        }

        public static void Remove(string key)
        {
            RedisHelper.RedisDatabase.HashDelete(_cacheServices, key);
            RedisHelper.RedisDatabase.HashDelete(_cacheServicesType, key);
        }

        public static List<string> GetAllKey()
        {
            return RedisHelper.RedisDatabase.HashGetAll(_cacheServices).Select(i=>i.Name.ToString()).ToList();
        }
    }
}
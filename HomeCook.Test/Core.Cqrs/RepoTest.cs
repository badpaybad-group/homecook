﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Cqrs.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HomeCook.Test.Core.Cqrs
{
    [TestClass]
    public class RepoTest
    {
        public class Test : AggregateRoot
        {
            public Test()
            {
            }

            public Test(Guid id, string title, string desciption)
            {
                Id = id;
                ApplyChange(new CreatedTest(Id, title, desciption));
            }

            public void Apply(CreatedTest e)
            {
                Id = e.Id;
            }

            public void ChangeTitle(string newTitle)
            {
                ApplyChange(new ChangedTitleTest(Id, newTitle));
            }


            public void ChangeTitle2(string newTitle)
            {
                ApplyChange(new ChangedTitleTest(Id, newTitle +" changetitle2"));
            }

            public void Apply(ChangedTitleTest e)
            {
            }
        }

        [TestMethod]
        public void EventSourcingTest()
        {
            ICqrsEventSourcingRepository<Test> _repository = new CqrsEventSourcingRepository<Test>();
            var id = Guid.NewGuid();
            _repository.CreateNew(new Test(id, "title 1", "description 1"));

            _repository.Save(id, test =>
            {
                test.ChangeTitle("title 1.1");
                test.ChangeTitle2("hello");
            });
        }
    }

    public class ChangedTitleTest : BaseEvent
    {
        public string NewTitle { get; }

        public ChangedTitleTest(Guid id, string newTitle):base(id)
        {
            NewTitle = newTitle;
        }
    }

    public class CreatedTest : BaseEvent
    {
        public string Title { get; }
        public string Desciption { get; }

        public CreatedTest(Guid id, string title, string desciption): base(id)
        {
            Title = title;
            Desciption = desciption;
        }
    }
}
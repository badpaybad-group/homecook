﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Command.Category;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HomeCook.Test.Data
{
    [TestClass]
   public class MySqlRepoTest
    {
        [TestMethod]
        public void Update()
        {
            MySqlCommandRepository<User> userRepo=new MySqlCommandRepository<User>();

            var entities = new User(){Id= Guid.Parse("de5d16d2-6387-4d1a-a9f6-937e28266bad"), Fullname= "testxxx",};

            userRepo.Update(entities);
        }
    }
}

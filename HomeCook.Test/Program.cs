﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HomeCook.Command;
using HomeCook.Core.Cqrs;
using HomeCook.Test.DomainUser;

namespace HomeCook.Test
{
   public class Program
    {
        public static void Main()
        {
           new DomainUserTest().CreateUser();

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HomeCook.Command.User;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data;
using HomeCook.Core.Data.MySql;
using HomeCook.Core.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HomeCook.Test.DomainUser
{
    [TestClass]
   public class DomainUserTest
    {
        public DomainUserTest()
        {
            CqrsCommandHandlerManager.Boot();
            CqrsEventHandlerManager.Boot();
            

           new DatabaseInitializer<HomeCookDbContext>().InitializeDatabase(new HomeCookDbContext());
        }

        [TestMethod]
        public void CreateUser()
        {
            var userid = Guid.NewGuid();
            Console.WriteLine(userid);
            var _1stPwd = "123456";
            CommandSender.Send(new CreateUserCommand(userid, "badpaybad",_1stPwd.ToPassword(),"badpaybad@gmail.com","01228384839"){}, message =>
            {
                Console.WriteLine("CreateSuccess: "+ message.IsSuccess);
            });
            
            Thread.Sleep(5000);

           // ChangePassword(userid);
        }

        [TestMethod]
        public void ChangePassword(Guid userid)
        {
            //var userid = Guid.Parse("6d7ac0a2-9b13-4e14-8f31-57231ca91426");
            var _1stPwd = "du123~!@";
          
            CommandSender.Send(new ChangeUserPasswordCommand(userid, "badpaybad", "123456".ToPassword(), _1stPwd), message =>
            {
                Console.WriteLine("ChangePasswordSuccess: " + message.IsSuccess);
            });

            Thread.Sleep(5000);
        }


    }
}

﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command
{
    public class TestCommand : Core.Cqrs.BaseCommand
    {
        public string SayHello { get; set; }

        public TestCommand(Guid id) : base(id)
        {
        }
    }
}
﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class VerifyUserEmailCommand : Core.Cqrs.BaseCommand
    {
        public string VerifyCode { get; set; }
        public string UserEmail { get; set; }

        public VerifyUserEmailCommand(Guid id) : base(id)
        {
        }
    }
}
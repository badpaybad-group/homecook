﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class VerifyUserPhoneCommand : Core.Cqrs.BaseCommand
    {
        public string VerifyCode { get; set; }
        public string UserPhone { get; set; }

        public VerifyUserPhoneCommand(Guid id) : base(id)
        {
        }
    }
}

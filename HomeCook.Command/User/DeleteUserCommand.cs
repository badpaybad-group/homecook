﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class DeleteUserCommand : Core.Cqrs.BaseCommand
    {
        public DeleteUserCommand(Guid id) : base(id)
        {
        }
    }
}
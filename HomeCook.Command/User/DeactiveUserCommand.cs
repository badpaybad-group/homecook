﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class DeactiveUserCommand : Core.Cqrs.BaseCommand
    {
        public DeactiveUserCommand(Guid id) : base(id)
        {
        }
    }
}
using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class CreateUserCommand:Core.Cqrs.BaseCommand
    {
        public CreateUserCommand(Guid id,string username, string password, string email, string phone):base(id)
        {
            Username = username;
            Password = password;
            Email = email;
            Phone = phone;
        }

        public string Username { get;  }
        public string Password { get;  }

        public string Email { get;  }
        public string Phone { get;  }
    }
}
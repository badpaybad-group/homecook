using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class SendEmailActiveUser : Core.Cqrs.BaseCommand
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string VerifyCode { get; set; }

        public SendEmailActiveUser(Guid id) : base(id)
        {
        }
    }
}
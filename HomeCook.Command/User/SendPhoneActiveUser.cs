﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class SendPhoneActiveUser : Core.Cqrs.BaseCommand
    {
    
        public string Username { get; set; }
        public string Phone { get; set; }
        public string VerifyCode { get; set; }

        public SendPhoneActiveUser(Guid id) : base(id)
        {
        }
    }
}
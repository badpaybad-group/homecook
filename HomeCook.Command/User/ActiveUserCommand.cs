using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class ActiveUserCommand : Core.Cqrs.BaseCommand
    {
        public string VerifyCode { get; set; }
        public string Username { get; set; }

        public ActiveUserCommand(Guid id) : base(id)
        {
        }
    }
}
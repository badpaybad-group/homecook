﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.User
{
    public class ChangeUserPasswordCommand : Core.Cqrs.BaseCommand
    {
        public ChangeUserPasswordCommand(Guid id,string username, string password, string oldPassword):base(id)
        {
            Username = username;
            Password = password;
            OldPassword = oldPassword;
        }

        public string Username { get; }
        public string Password { get; }
        public string OldPassword { get;  }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Command
{
    public class CreateEntity<TEntity>: Core.Cqrs.BaseCommand where TEntity: BaseEntity
    {
       public readonly TEntity[] Entity;

       public CreateEntity(params TEntity[] entity):base(Guid.Empty)
       {
           Entity = entity;
    
        }
    }

    public class CreateOrUpdateEntity<TEntity> : Core.Cqrs.BaseCommand where TEntity : BaseEntity
    {
        public readonly TEntity[] Entity;

        public CreateOrUpdateEntity(params TEntity[] entity) : base(Guid.Empty)
        {
            Entity = entity;
     
        }
        
    }

    public class UpdateEntity<TEntity> : Core.Cqrs.BaseCommand where TEntity : BaseEntity
    {
        public readonly TEntity[] Entity;

        public UpdateEntity(params TEntity[] entity) : base(Guid.Empty)
        {
            Entity = entity;
          
        }
    }
   
    public class DeleteEntity<TEntity> : Core.Cqrs.BaseCommand where TEntity : BaseEntity
    {
        public readonly TEntity[] Entity;

        public DeleteEntity(params TEntity[] entity) : base(Guid.Empty)
        {
            Entity = entity;
           
        }
    }

    public class DeleteEntityWithCondition<TEntity> : Core.Cqrs.BaseCommand where TEntity : BaseEntity
    {
        public readonly Expression<Func<TEntity, bool>> Predicate;

        public DeleteEntityWithCondition(Expression<Func<TEntity, bool>> predicate) : base(Guid.Empty)
        {
            Predicate = predicate;
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Cqrs;

namespace HomeCook.Command.Category
{
   public class CreateCategory:BaseCommand
    {
        public CreateCategory(Guid id) : base(id)
        {
        }

        public string Name { get; set; }
        public string IconUrl { get; set; }
        public string Description { get; set; }
        public Guid ParentId { get; set; }
    }

    public class UpdateCategory:BaseCommand
    {
        public UpdateCategory(Guid id) : base(id)
        {
        }

        public string Name { get; set; }
        public string IconUrl { get; set; }
        public string Description { get; set; }
        public Guid ParentId { get; set; }
    }

    public class DeleteCategory : BaseCommand {
        public DeleteCategory(Guid id) : base(id)
        {
        }
    }
}

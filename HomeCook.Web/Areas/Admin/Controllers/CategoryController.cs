﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;

namespace HomeCook.Web.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        IQueryFacade<Category> _categoryQueryFacade=new MySqlQueryFacade<Category>();
        
        public ActionResult Index()
        {
            return View();
        }

       
        public JsonResult Search(string keywords, int type, int roleId, int skip, int take, string sortField, string orderBy)
        {
            var ut = type;
            int total = 0;

            Expression<Func<Category, bool>> expression = user => true;
           

            var rows = _categoryQueryFacade.SelectBy(expression, sortField, orderBy, skip, take, out total);

            return Json(new { total, rows, success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HomeCook.Command.User;
using HomeCook.Core.Cqrs;

namespace HomeCook.Web.Areas.Admin.Controllers
{
    public class NotificationTestController : Controller
    {
        // GET: Admin/NotificationTest
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult Send(string msg, string clientId)
        {
            try
            {

                CqrsNotificationServices.Push(new CommandResultNotificationMessage(
                    new CreateUserCommand( Guid.NewGuid(), "test", "test", "test", "test"
                    ){ClientId = clientId})
                {
                    IsSuccess = true,
                    Message = msg
                });

                return Json(new
                {
                    Ok = true,
                    Message = "Da gui command  "
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Json(new
                {
                    Ok = true,
                    Message = e.Message
                }, JsonRequestBehavior.AllowGet);
            }
          
        }

        
    }
}
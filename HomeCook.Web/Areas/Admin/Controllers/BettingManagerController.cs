﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;

namespace HomeCook.Web.Areas.Admin.Controllers
{
    public class BettingManagerController : Controller
    {
        // GET: Admin/BettingManager
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListCategory(string q, int? page)
        {
            List<Category> list=new List<Category>();
            Expression<Func<Category, bool>> expression = i => true;
            if (!string.IsNullOrEmpty(q))
            {
                expression = i => i.Name.Contains(q);
            }
            using (var db = new HomeCookDbContext())
            {
                var query = db.Set<Category>().Where(expression);
                list = query.ToList();
            }

            return Json(new
            {
                total_count = list.Count,
                incomplete_results = false,
                items = list.Select(i => new { id = i.Id, text = i.Name  })
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System.Web.Mvc;
using HomeCook.Core.Mvc;

namespace HomeCook.Web.Areas.Admin.Controllers
{
    [LoginRequired]
    public class AdminHomeController : Controller,IRightMappingController
    {
        // GET: Admin/AdminHome
        public ActionResult Index()
        {
            return View();
        }
    }
}
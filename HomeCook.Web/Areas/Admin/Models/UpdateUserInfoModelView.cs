﻿using System;
using System.Collections.Generic;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Web.Areas.Admin.Models
{
    public class UpdateUserInfoModelView
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IdentifyCard { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public string University { get; set; }

        public int ProfileImgId { get; set; }

        public string JobTitle { get; set; }

        public string Course { get; set; }
        public string Faculty { get; set; }
    
        public List<Role> Roles { get; set; } 
    }

    public class ChangeUserInfoModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string IdentifyCard { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public string University { get; set; }
        
        public string JobTitle { get; set; }

        public string Course { get; set; }
        public string Faculty { get; set; }
        
    }

    public class ChangePasswordUserInfoModel
    {
        public int Id { get; set; }
        public string Password { get; set; }
    }
}
﻿using System.Collections.Generic;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Web.Areas.Admin.Models
{
    public class AdminRoleRightModel
    {
        public List<Role> Roles { get; set; }
        public List<Right> Rights { get; set; }  
    }
}
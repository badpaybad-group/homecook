﻿Category = {
    $tblList: null,
    $txtSearch: null,
   
    initListLogTbl: function ($tblList, $txtSearch) {
        Category.$txtSearch = $txtSearch;
        Category.$tblList = $tblList;

        Category.$tblList.bootstrapTable({
            idField: 'Id',
            //data: AdminUser.arrayCheckedIds,
            method: 'post',
            url: '/Category/Search?__=' + (new Date().getTime()),
            sidePagination: 'server',
            pagination: true,
            showToggle: false,
            minimumCountColumns: 1,
            pageSize: 10,
            pageList: [10, 20, 50, 100, 200],
            showColumns: false,
            showRefresh: false,
            sortName: 'CreatedAt',
            sortOrder: 'desc',
            queryParams: function (p) {
                var keywords = '';
                if (Category.$txtSearch) {
                    keywords = Category.$txtSearch.val();
                }
                return {
                    sortField: p.sort,
                    orderBy: p.order,
                    take: p.limit,
                    skip: p.offset,
                    keywords:keywords
                };
            },
            columns: [
                {
                    field: 'TaskName',
                    title: 'Activities',
                    align: 'left',
                    valign: 'middle',
                    formatter: function(value, row) {
                        var user = '<a onclick="RightPanel.ShowUserToEdit(\'' + row.UserId + '\')" title="' + row.UserName + '">'
                            + row.UserFullName + '</a> '; 

                
                        var openComment = false;
                        if (row.ActionType == 1 || row.ActionType == 3
                            || row.ActionType == 3 || row.ActionType == 6
                            || row.ActionType == 7 || row.ActionType == 8) {
                            openComment = true;
                        }
                        var temp = user + row.ActionTypeText + ' on task <a   ' +
                            'onclick="RightPanel.ShowTaskInfoPanel(\'' + row.TaskId + '\',false,'+openComment+')" >' +
                            row.TaskName + '</a> ' +Utility.JsonDateParse(row.CreatedAt);

                        return temp;
                    }
                }
            ]
        });

    },
    Search: function () {
        if (Category.$tblList)
            Category.$tblList.bootstrapTable('refresh');
    }
}
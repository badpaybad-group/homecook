using System;
using System.Collections.Generic;
using System.Web;
using HomeCook.Core.Config;
using HomeCook.Core.Data.Entity;

namespace HomeCook.Web
{
    public class HttpSessionUserManager
    {
        const string CustomerSessionKey = "__CustomerSessionKey";

        //public static void Init(IDistributedServices distributedServices, HttpContext context, IUserRoleRightRepository roleRightRepository)
        //{
        //    distributedServices.Subscribe<UserLoginSuccess>((command) =>
        //    {
        //        List<Right> rights=roleRightRepository.ListRightByUser(command.Data.UserInfo.Id);
        //        List<Role> roles=roleRightRepository.ListRoleByUser(command.Data.UserInfo.Id);
        //        context.Session[CustomerSessionKey] = new UserSession(command.Data.SessionId, command.Data.UserInfo, roles,rights);
        //    });

        //    distributedServices.Subscribe<UserLogoutSuccess>((cmd) =>
        //    {
        //        context.Session[CustomerSessionKey] = null;
        //    });
        //}
        
        public static UserSession Current()
        {
            var session = HttpContext.Current.Session[CustomerSessionKey];
            return session as UserSession;
        }

        public static bool CurrentUserIsAdmin()
        {
            var u = Current();
            if (u == null) return false;
            return u.IsSupperAdmin;
        }

        public static Guid CurrentUserId()
        {
            var u = Current();
            if (u == null) return Guid.Empty;
            return u.User.Id;
        }

        public static string CurrentToken()
        {
            var u = Current();
            if (u == null) return string.Empty;
            return u.Token;
        }

        public static void SetSession(UserSession session)
        {
            HttpContext.Current.Session[CustomerSessionKey] = session;
            
            HttpContext.Current.Response.Cookies.Set(new HttpCookie("homecook_token",session==null?string.Empty: session.Token));
        }
        
        public class UserSession
        {
            public string Token { get; }
            public User User { get; }

            public List<Role> Roles { get; }
            public List<Right> Rights { get; }


            private bool? _isSupperAdmin;

            public UserSession(string token, User user, List<Role> roles, List<Right> rights)
            {
                Token = token;
                User = user;
                Roles = roles;
                Rights = rights;
            }

            public bool IsSupperAdmin
            {
                get
                {
                    if (_isSupperAdmin != null)
                    {
                        return _isSupperAdmin.Value;
                    }

                    if (User == null)
                    {
                        _isSupperAdmin = false;
                        return false;
                    }
                    if (User.Username.Equals(SystemConfig.AdminUsername, StringComparison.OrdinalIgnoreCase))
                    {
                        _isSupperAdmin = true;
                        return true;
                    }

                    if (Roles == null || Roles.Count == 0)
                    {
                        _isSupperAdmin = false;
                        return false;
                    }

                    //if (Roles.Select(i => i.Name.ToLower()).Contains(SystemConfig.Role.Admin.ToString().ToLower()))
                    //{
                    //    _isSupperAdmin = true;
                    //    return true;
                    //}

                    _isSupperAdmin = false;
                    return false;
                }
            }
        }
    }
}
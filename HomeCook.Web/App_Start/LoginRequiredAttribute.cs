﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeCook.Core.Config;

namespace HomeCook.Web
{
    public class LoginRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //HttpContextBase context = filterContext.HttpContext;

            //var loged = HttpSessionUserManager.Current();

            //var url = context.Request.Url.ToString().ToLower();
            //var customerLoginUrl = SystemConfig.UrlLogin + "?url=" + HttpUtility.UrlEncode(url);

            //var controller = filterContext.RouteData.GetRequiredString("controller");
            //var action = filterContext.RouteData.GetRequiredString("action");

            //if (loged == null)
            //{
            //    filterContext.Result = new RedirectResult(customerLoginUrl);
            //}
            //else
            //{
            //    if (!loged.IsSupperAdmin)
            //    {
            //        if (!loged.Rights.Any(i =>
            //        {
            //            var controllerAction = "/" + controller + "/" + action;
            //            return controllerAction.IndexOf(i.Name, StringComparison.OrdinalIgnoreCase) >= 0;
            //        }))
            //        {
            //            filterContext.Result = new RedirectResult(customerLoginUrl);
            //        }
            //    }
            //}

            //base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }

    public static class HttpRequestHelper
    {
        public static string GetRootDomain()
        {
            var domain = HttpContext.Current.Request.Url.Host.Trim('/');
            if (domain.IndexOf("localhost", StringComparison.OrdinalIgnoreCase) >= 0 ||
                domain.IndexOf("127.0.0.1", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                domain = domain + ":" + HttpContext.Current.Request.Url.Port;
            }

            return "http://" + domain.Trim('/');
        }
    }
}
﻿using HomeCook.Core.Cqrs;
using HomeCook.Core.Redis;
using HomeCook.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(StartupSignalR))]
namespace HomeCook.Web
{

    [HubName("notificationSignalR")]
    public class NotificationSignalR : Hub
    {
        public void CqrsCommandSubscribe(string commandTypeFullName)
        {
            CqrsNotificationServices.Subscribe(commandTypeFullName, message =>
            {
                Clients.Caller.NotifyFromCommand(message);
            } );
        }

        public void CqrsCommandUnsubscribe(string commandTypeFullName)
        {
            CqrsNotificationServices.UnSubscribe(commandTypeFullName);
        }
    }
}
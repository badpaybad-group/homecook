using Owin;

namespace HomeCook.Web
{
    public class StartupSignalR
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}
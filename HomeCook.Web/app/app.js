﻿Utility= {
    JsonDateParse:function(jsondate) {
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = new Date(parseInt(jsondate.substr(6)));
        var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
        var time = d.toLocaleTimeString().toLowerCase();
        var tempDate = date + ' at ' + time;
        return tempDate;
    }
}
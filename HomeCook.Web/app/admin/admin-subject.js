﻿AdminSubject = {
    $tblView:null,
    init: function ($tblView) {
        this.$tblView = $tblView;
        this.$tblView.bootstrapTable({
            idField: 'Id',
            method: 'post',
            url: '/Admin/AdminSubject/Search',
            sidePagination: 'server',
            pagination: true,
            showToggle: true,
            minimumCountColumns: 1,
            pageSize: 50,
            pageList: [10, 25, 50, 100, 200],
            showColumns: true,
            showRefresh: true,
            sortName: 'Code',
            sortOrder: 'desc',
            queryParams: function(p) {
            
                return {
                    sortField: p.sort,
                    orderBy: p.order,
                    take: p.limit,
                    skip: p.offset
                };
            },
            columns: [
                {
                    field: 'Code',
                    title: 'Code',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'Name',
                    title: 'Name',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'StartDate',
                    title: 'StartDate',
                    align: 'left',
                    valign: 'middle',
                    sortable: true,
                    formatter: function (value, row) {
                       
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        var d = new Date(parseInt(row.StartDate.substr(6)));
                        var date = d.getDay() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();
                        return date + ' at ' + time;
                    }
                }, {
                    field: 'EndDate',
                    title: 'EndDate',
                    align: 'left',
                    valign: 'middle',
                    sortable: true,
                    formatter: function (value, row) {
                       
                        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        var d = new Date(parseInt(row.EndDate.substr(6)));
                        var date = d.getDay() + " " + month[d.getMonth()] + ", " + d.getFullYear();
                        var time = d.toLocaleTimeString().toLowerCase();
                        return date + ' at ' + time;
                    }
                }, {
                   field: 'Functions',
                   title: 'Functions',
                   align: 'center',
                   valign: 'middle',
                   formatter: function (value, row) {
                       var temp = '<a class="tbl-function fa fa-remove" title="Remove" onclick="AdminSubject.RemoveFromSelected(\'' + row.Id + '\')"><span> Remove</span></a> &nbsp;';
                       temp += '<a href="/Admin/AdminSubject/Update/' + row.Id + '" class="tbl-function fa fa-edit" title="Edit" ><span> Edit</span></a> ';
                       return temp;
                   }
               }]
        });
    },
    RemoveFromSelected: function (id) {
        bootbox.confirm('Do you want to remove?', function (result) {

            if (!result) return;

            $.post('/Admin/AdminSubject/Remove',
                {
                    id: id
                })
                .done(function(data) {
                    if (data.Ok) {
                        toastr.success("Remove success", "Remove success");

                        AdminSubject.$tblView.bootstrapTable('refresh');
                    } else {
                        toastr.error(data.Message);
                    }
                }).fail(function () {
                    bootbox.alert({
                        message: "Can not make request, check your internet and try to reload page",
                        backdrop: true
                    });
                });
        });
     
    }
}
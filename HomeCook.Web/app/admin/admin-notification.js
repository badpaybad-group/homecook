﻿CqrsCommandNotification = {
    _notifyHub: null,
    _cqrsCmdMsg: [],
    _isStarted:false,
    init:function() {
        CqrsCommandNotification._notifyHub = $.connection.notificationSignalR;

        CqrsCommandNotification._notifyHub.client.NotifyFromCommand = function (msg) {

            if (!CqrsCommandNotification._cqrsCmdMsg[msg.CommandTypeFullName]) {
                CqrsCommandNotification._cqrsCmdMsg[msg.CommandTypeFullName] = [];
            } else {
                CqrsCommandNotification._cqrsCmdMsg[msg.CommandTypeFullName].push(msg);
            }

            if (msg.IsSuccess) {
                 toastr.success(msg.Message, "Thành công");
            } else {
                 toastr.error(msg.Message,"Lỗi");
               
            }
        };

        $.connection.hub.start()
            .done(function () {
                CqrsCommandNotification._isStarted = true;
            });

        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start();
            }, 5000); // Re-start connection after 5 seconds
        });
    },
    subscribe: function (commandTypeFullName, timeOutSubscribe) {
        
        if (CqrsCommandNotification._isStarted) {
                 CqrsCommandNotification._notifyHub.server.cqrsCommandSubscribe(commandTypeFullName);
                 if (timeOutSubscribe) {
                     clearTimeout(timeOutSubscribe);
                 }
                 return;
        }

        timeOutSubscribe = setTimeout(function () {
        CqrsCommandNotification.subscribe(commandTypeFullName, timeOutSubscribe);

        },1000);
    },
    unSubscribe:function( commandTypeFullName) {
        CqrsCommandNotification._notifyHub.server.cqrsCommandUnsubscribe(commandTypeFullName);
    }
}
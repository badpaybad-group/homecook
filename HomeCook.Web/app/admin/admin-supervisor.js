﻿AdminSupervisor = {
    $selectionSubject:null,
    $selectionTeacher: null,
    $tblAllStudent: null,
    $tblStudentBelongToTeacher: null,
    studentsSelected: [],
    enumTeacher: 0,
    enumStudent: 0,
    studentsLeft:[],
    studentsRight: [],
    $divStudent: null,
    imgLoading:null,
    init: function ($selectionSubject,$selectionTeacher, $tblAllStudent, $tblStudentBelongToTeacher
        , enumTeacher, enumStudent, $divStudent) {
        this.imgLoading = "<img alt='loading...' src='/Content/imgs/loading.gif'/> loading...";
        this.$selectionSubject = $selectionSubject;
        this.$divStudent = $divStudent;
        this.enumStudent = enumStudent;
        this.enumTeacher = enumTeacher;
        this.$tblAllStudent = $tblAllStudent;
        this.$tblStudentBelongToTeacher = $tblStudentBelongToTeacher;
       
        this.$selectionTeacher = $selectionTeacher;

        this.$selectionTeacher.selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });


        this.$selectionSubject.selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });

        AdminSupervisor.studentsSelected = [];
        AdminSupervisor.initTblAllStudent();
        AdminSupervisor.tblStudentBelongToTeacher();
    },
    StudentExisted:function(arr, studId) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].Id == studId) return true;
        }
        return false;
    },
    initTblAllStudent:function() {
        AdminSupervisor.$tblAllStudent.bootstrapTable({
            idField: 'Id',
            //data: AdminUser.arrayCheckedIds,
            method: 'post',
            url: '/Admin/AdminSupervisor/FindUsersByRole?__=' + (new Date().getTime()),
            sidePagination: 'server',
            pagination: true,
            showToggle: false,
            minimumCountColumns: 1,
            pageSize: 10,
            pageList: [10, 20, 50, 100, 200],
            showColumns: false,
            showRefresh: false,
            sortName: 'Username',
            sortOrder: 'desc',
            queryParams: function (p) {
                return {
                    sortField: p.sort,
                    orderBy: p.order,
                    take: p.limit,
                    skip: p.offset,
                    role: AdminSupervisor.enumStudent,
                    
                };
            },
            columns: [
                 {
                     field: 'RowChecked',
                     title: '',
                     align: 'left',
                     valign: 'middle',
                     checkbox: true
                 }, {
                    field: 'FirstName',
                    title: 'Students',
                    align: 'left',
                    valign: 'middle',
                    sortable: true,
                    formatter:function(value, row) {
                        return '<a onclick="AdminSupervisor.OnSelectStudent(\'' + row.Id + '\');' +
                            'RightPanel.ShowUserToEdit(\'' + row.Id + '\')"  >' +
                            row.FirstName + " " + row.LastName + " (" + row.IdentifyCard + ")"
                        +'</a>';
                    }
                 }
                //, {
                //    field: '',
                //    formatter:function(value, row) {
                //        var temp = '<a onclick="AdminSupervisor.OnSelectStudent(\'' + row.Id + '\')" class="fa fa-angle-double-up">view</a> ';
                //        temp += ' <a onclick="RightPanel.ShowUserToEdit(\'' + row.Id + '\')" class="fa fa-edit" >Edit</a>';
                //        return temp;
                //    }
                //}
            ],
            onCheckAll: function (rows) {
                for (var i = 0; i < rows.length; i++) {
                    if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsLeft, rows[i].Id)) {
                        AdminSupervisor.studentsLeft.push(rows[i]);
                    }
                }
            },
            onUncheckAll: function (rows) {
                var keepItems = [];
                 
                    for (var k = 0; k < AdminSupervisor.studentsLeft.length; k++) {
                        if (!AdminSupervisor.StudentExisted(rows, AdminSupervisor.studentsLeft[k].Id)) {
                            keepItems.push(AdminSupervisor.studentsLeft[k]);
                        }
                    }
                AdminSupervisor.studentsLeft = keepItems;

            },
            onCheck: function (row) {
               
                if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsLeft, row.Id)) {
                    AdminSupervisor.studentsLeft.push(row);
                }
            },
            onUncheck: function (row) {
                var keepItems = [];

                for (var k = 0; k < AdminSupervisor.studentsLeft.length; k++) {
                    if (!AdminSupervisor.StudentExisted( AdminSupervisor.studentsLeft, row.Id)) {
                        keepItems.push(AdminSupervisor.studentsLeft[k]);
                    }
                }
                AdminSupervisor.studentsLeft = keepItems;
            }
        });

    }
    ,
    tblStudentBelongToTeacher:function() {
        AdminSupervisor.$tblStudentBelongToTeacher.bootstrapTable({
            idField: 'Id',
            data: AdminSupervisor.studentsSelected,
            pagination: false,
            showToggle: false,
            minimumCountColumns: 1,
            pageSize: 10,
            pageList: [10, 20, 50, 100, 200],
            showColumns: false,
            showRefresh: false,
            sortName: 'Username',
            sortOrder: 'desc',
            columns: [
                 {
                     field: 'RowChecked',
                     title: '',
                     align: 'left',
                     valign: 'middle',
                     checkbox: true
                 },
               {
                   field: 'FirstName',
                   title: 'Assigned',
                   align: 'left',
                   valign: 'middle',
                   sortable: true,
                   formatter: function (value, row) {
                     return  row.FirstName + " " + row.LastName + " (" + row.IdentifyCard + ")";
                   }
               }
               //,
               //{
               //    field: '',
               //    title: '',
               //    align: 'left',
               //    valign: 'middle',
               //    sortable: true,
               //    formatter: function (value, row) {
               //        var teacherId = parseInt(AdminSupervisor.$selectionTeacher.val());
               //        var subjectId = parseInt(AdminSupervisor.$selectionSubject.val());
                     
               //        var temp = '<a class="tbl-function fa fa-remove" title="Remove" ' +
               //            'onclick="AdminSupervisor.RemoveStudent' +
               //            '(\''+subjectId+'\',\''+teacherId+'\',\'' + row.Id + '\')">' +
               //            '<span> Remove</span></a> &nbsp;';

               //        return temp;
               //    }
               //}
            ],
            onCheckAll: function (rows) {
                for (var i = 0; i < rows.length; i++) {
                    if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsRight, rows[i].Id)) {
                        AdminSupervisor.studentsRight.push(rows[i]);
                    }
                }
            },
            onUncheckAll: function (rows) {
                var keepItems = [];

                for (var k = 0; k < AdminSupervisor.studentsRight.length; k++) {
                    if (!AdminSupervisor.StudentExisted(rows, AdminSupervisor.studentsRight[i].Id)) {
                        keepItems.push(AdminSupervisor.studentsRight[i]);
                    }
                }
                AdminSupervisor.studentsRight = keepItems;

            },
            onCheck: function (row) {
                if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsRight, row.Id)) {
                    AdminSupervisor.studentsRight.push(row);
                }
            },
            onUncheck: function (row) {
                var keepItems = [];

                for (var k = 0; k < AdminSupervisor.studentsRight.length; k++) {
                    if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsRight, row.Id)) {
                        keepItems.push(AdminSupervisor.studentsRight[i]);
                    }
                }
                AdminSupervisor.studentsRight = keepItems;
            }
        });
    },
    MoveSelectedLeftToRight:function() {
        for (var i = 0; i < AdminSupervisor.studentsLeft.length; i++) {
            if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsSelected, AdminSupervisor.studentsLeft[i].Id)) {
                var studentsLeft = AdminSupervisor.studentsLeft[i];
                studentsLeft.RowChecked = false;
                AdminSupervisor.studentsSelected.push(studentsLeft);
            }
        }
       
        AdminSupervisor.studentsLeft = [];
        AdminSupervisor.$tblAllStudent.bootstrapTable('refresh');
        AdminSupervisor.$tblStudentBelongToTeacher.bootstrapTable('load', AdminSupervisor.studentsSelected);

    },
    MoveSelectedRightToLeft: function () {
        var keepItems = [];
        for (var i = 0; i < AdminSupervisor.studentsSelected.length; i++) {
            if (!AdminSupervisor.StudentExisted(AdminSupervisor.studentsRight, AdminSupervisor.studentsSelected[i].Id)) {
                var studentsSelected = AdminSupervisor.studentsSelected[i];

                keepItems.push(studentsSelected);
            }
        }
        AdminSupervisor.studentsSelected = keepItems;
        AdminSupervisor.studentsRight = [];
        AdminSupervisor.$tblAllStudent.bootstrapTable('refresh');
        AdminSupervisor.$tblStudentBelongToTeacher.bootstrapTable('load', AdminSupervisor.studentsSelected);

    },
   
    OnSelectSubject:function($sender,$drlTeacher) {
        //$drlTeacher.val("-1");
    },
    OnSelectTeacher:function($sender,$divTeacher) {
        var teacherId = parseInt($sender.val());
        var subjectId = parseInt(AdminSupervisor.$selectionSubject.val());

        if (teacherId <= 0) {
            AdminSupervisor.studentsSelected = [];
            AdminSupervisor.studentsRight = [];
            AdminSupervisor.studentsLeft = [];
            AdminSupervisor.$tblStudentBelongToTeacher.bootstrapTable('load', AdminSupervisor.studentsSelected);
            return;
        }
        $divTeacher.html(AdminSupervisor.imgLoading);
        $.post('/Admin/AdminSupervisor/PartialFindUser',
             {
                 id: teacherId
             })
             .done(function (data) {
                 $divTeacher.html(data);
             })
         .fail(function () {
             bootbox.alert({
                 message: "Can not make request, check your internet and try to reload page",
                 backdrop: true
             });
         });

        $.post('/Admin/AdminSupervisor/FindUserRelation',
            {
                teacherId: teacherId,
                subjectId: subjectId
            })
            .done(function (data) {
                if (data.Ok) {
                    AdminSupervisor.studentsSelected = data.Data;
                    AdminSupervisor.$tblStudentBelongToTeacher.bootstrapTable('load', AdminSupervisor.studentsSelected);

                }
                //var arr = [];

                //for (var i = 0; i < data.Data.length; i++) {
                //    arr.push(data.Data[i].Id);
                
                //}
             
                //AdminSupervisor.$supervisorRelation.val(arr);
                //AdminSupervisor.$supervisorRelation.multiSelect("refresh");
            }).fail(function () {
                bootbox.alert({
                    message: "Can not make request, check your internet and try to reload page",
                    backdrop: true
                });
            });
    },
    SupervisoRelationSave: function () {
        var teacherId = parseInt(AdminSupervisor.$selectionTeacher.val());
        var subjectId = parseInt(AdminSupervisor.$selectionSubject.val());
        if (teacherId <= 0) {
            toastr.error("Must select teacher first");
            return;
        }
        var studentIds = [];
        for (var i = 0; i < AdminSupervisor.studentsSelected.length; i++) {
            studentIds.push(AdminSupervisor.studentsSelected[i].Id);
        }

        $.post('/Admin/AdminSupervisor/SupervisoRelationSave',
        {
            teacherId: teacherId,
            subjectId: subjectId,
              studentIds:studentIds
          })
          .done(function (data) {
                if (data.Ok) {
                    toastr.success("Add success", "Add success");
                } else {
                    toastr.error(data.Message);
                }
          }).fail(function () {
              bootbox.alert({
                  message: "Can not make request, check your internet and try to reload page",
                  backdrop: true
              });
          });
    }
    , OnSelectStudent: function (userId, $divStudent) {
        if (!$divStudent) {
            $divStudent = AdminSupervisor.$divStudent;
        }
        $divStudent.html(AdminSupervisor.imgLoading);
        $.post('/Admin/AdminSupervisor/PartialFindUser',
           {
               id: userId
           })
           .done(function (data) {
               $divStudent.html(data);
           }).fail(function () {
               bootbox.alert({
                   message: "Can not make request, check your internet and try to reload page",
                   backdrop: true
               });
           });
    },
    RemoveSubject: function(subjectId) {
        bootbox.confirm('Do you want to remove?', function(result) {
            if (!result) return;

            $.post('/Admin/AdminSupervisor/RemoveSubject',
                {
                    subjectId: subjectId
                })
                .done(function(data) {
                    toastr.success("Success", "Success");
                }).fail(function() {
                    bootbox.alert({
                        message: "Can not make request, check your internet and try to reload page",
                        backdrop: true
                    });
                });
        });

    },
    RemoveTeacher: function(subjectId, teacherId) {
        bootbox.confirm('Do you want to remove?', function(result) {
            if (!result) return;
            $.post('/Admin/AdminSupervisor/RemoveTeacher',
                {
                    subjectId: subjectId,
                    teacherId: teacherId
                })
                .done(function(data) {
                    toastr.success("Success", "Success");
                }).fail(function() {
                    bootbox.alert({
                        message: "Can not make request, check your internet and try to reload page",
                        backdrop: true
                    });
                });
        });

    },
    RemoveStudent: function(subjectId, teacherId, studentId) {
      
        bootbox.confirm('Do you want to remove?', function(result) {
            if (!result) return;
            $.post('/Admin/AdminSupervisor/RemoveStudent',
                {
                    subjectId: subjectId,
                    teacherId: teacherId,
                    studentId: studentId
                })
                .done(function (data) {

                    AdminSupervisor.studentsRight = [new{Id: studentId}];

                    AdminSupervisor.MoveSelectedRightToLeft();

                    toastr.success("Success", "Success");
                }).fail(function() {
                    bootbox.alert({
                        message: "Can not make request, check your internet and try to reload page",
                        backdrop: true
                    });
                });
        });


    },
    ReloadTblAllStudent: function() {
        AdminSupervisor.$tblAllStudent.bootstrapTable('refresh', {
            url: '/Admin/AdminSupervisor/FindUsersByRole?__=' + (new Date().getTime())
        });
    }
}
﻿AdminUser = {
    $tblView:null,
    init: function ($tblView) {
        this.$tblView = $tblView;
        this.$tblView.bootstrapTable({
            idField: 'Id',
            //data: AdminUser.arrayCheckedIds,
            method: 'post',
            url: '/Admin/AdminUser/Search',
            sidePagination: 'server',
            pagination: true,
            showToggle: true,
            minimumCountColumns: 1,
            pageSize: 50,
            pageList: [10, 25, 50, 100, 200],
            showColumns: true,
            showRefresh: true,
            sortName: 'Username',
            sortOrder: 'desc',
            queryParams: function(p) {
            
                return {
                    sortField: p.sort,
                    orderBy: p.order,
                    take: p.limit,
                    skip: p.offset
                };
            },
            columns: [
                {
                    field: 'Username',
                    title: 'Username',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'FirstName',
                    title: 'FirstName',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'LastName',
                    title: 'LastName',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'Email',
                    title: 'Email',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'Phone',
                    title: 'Phone',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                    field: 'University',
                    title: 'University',
                    align: 'left',
                    valign: 'middle',
                    sortable: true
                }, {
                   field: 'Functions',
                   title: 'Functions',
                   align: 'center',
                   valign: 'middle',
                   formatter: function (value, row) {
                       var temp = '<a class="tbl-function fa fa-remove" title="Remove" onclick="AdminUser.RemoveFromSelected(\'' + row.Id + '\')"><span> Remove</span></a> &nbsp;';
                       temp += '<a href="/Admin/AdminUser/Update/' + row.Id + '" class="tbl-function fa fa-edit" title="Edit" ><span> Edit</span></a> ';
                      // temp += '<a href="/Admin/AdminUser/Update/' + row.Id + '" class="tbl-function fa fa-edit" title="Edit roles" >Edit roles</a> ';
                       return temp;
                   }
               }]
        });
    },
    RemoveFromSelected: function (id) {
        bootbox.confirm('Do you want to remove?', function (result) {

            if (!result) return;

            $.post('/Admin/AdminUser/Remove',
                {
                    id: id
                })
                .done(function(data) {
                    if (data.Ok) {
                        toastr.success("Remove success", "Remove success");

                        AdminUser.$tblView.bootstrapTable('refresh');
                    } else {
                        toastr.error(data.Message);
                    }
                }).fail(function () {
                    bootbox.alert({
                        message: "Can not make request, check your internet and try to reload page",
                        backdrop: true
                    });
                });
        });
     
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeCook.Web.api.Response
{
    public class ApiResponse<T>
    {
        public ApiResponse(bool isSuccess, int errorCode, string errorMessage, T data)
        {
            IsSuccess = isSuccess;
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            Data = data;
        }

        public bool IsSuccess { get; }
        public int ErrorCode { get;  }
        public string ErrorMessage { get;  }
        public T Data { get;  }
    }
}
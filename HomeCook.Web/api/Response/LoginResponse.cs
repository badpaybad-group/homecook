﻿using HomeCook.Core.Data.Entity;

namespace HomeCook.Web.api.Response
{
    public class LoginResponse
    {
        public LoginResponse(string token, User user, bool success)
        {
            Token = token;
            User = user;
            Success = success;
        }
        public bool Success { get; }
        public string Token { get; }
        public User User { get; }
    }
}
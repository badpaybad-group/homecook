﻿namespace HomeCook.Web.api.Request
{
    public class BaseRequest
    {
        public BaseRequest(string token)
        {
            Token = token;
        }

        public string Token { get; }
    }
}
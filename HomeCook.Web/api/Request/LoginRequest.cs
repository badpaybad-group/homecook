﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeCook.Web.api.Request
{
    public class LoginRequest
    {
        public LoginRequest(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public string Username { get; }
        public string Password { get; }
    }
}
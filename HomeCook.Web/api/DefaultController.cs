﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using HomeCook.Core.Mvc;
using Newtonsoft.Json;

namespace HomeCook.Web.api
{
    public class DefaultController : ApiController, IRightMappingController
    {
       
        public string Get()
        {
            return "version 1.0";
        }

       
    }
}

﻿using System.Web.Http;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;
using HomeCook.Core.Mvc;
using HomeCook.Core.Services;
using HomeCook.Web.api.Request;
using HomeCook.Web.api.Response;

namespace HomeCook.Web.api
{
    public class SessionController : ApiController, IRightMappingController
    {
        private UserSessionServices _userSessionServices;
        private IQueryFacade<User> _userQueryFacade;

        public SessionController()
        {
             _userQueryFacade = new MySqlQueryFacade<User>();
            _userSessionServices = new UserSessionServices(_userQueryFacade);
        }

        [HttpPost]
        public ApiResponse<LoginResponse> Login(LoginRequest request)
        {
            User u;
            string sessionId;
            var ok = _userSessionServices.DoLogin(request.Username, request.Password, out sessionId, out u);

            return new ApiResponse<LoginResponse>(ok, ok ? 0 : -1, ok ? "Success" : "Fail",
                new LoginResponse(sessionId, u, ok));
        }

        public ApiResponse<bool> Dologout(BaseRequest request)
        {
            _userSessionServices.DoLogout(request.Token);

            return new ApiResponse<bool>(true, 0, "", true);
        }

        public ApiResponse<bool> IsValidToken(BaseRequest request)
        {
            var ok = _userSessionServices.IsAlive(request.Token);

            return new ApiResponse<bool>(ok, ok ? 0 : -1, "", ok);
        }
    }
}
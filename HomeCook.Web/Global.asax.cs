﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using HomeCook.Command;
using HomeCook.Core;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;
using HomeCook.Core.Distributed;
using HomeCook.Core.Mvc;
using HomeCook.Core.Redis;
using HomeCook.Core.Services;
using StructureMap;

namespace HomeCook.Web
{
    public class Global : HttpApplication
    {
        Container _container = new Container(r =>
        {
            //r.AddRegistry<IocCoreRegister>();
            //r.AddRegistry<IocQueryFacadeRegister>();
            //r.AddRegistry<IocServicesRegister>();
        });

        void Application_Start(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += AppDomain_UnhandledException;

            var initdb = new CreateDatabaseIfNotExists<HomeCookDbContext>();
            initdb.InitializeDatabase(new HomeCookDbContext());
            
            RedisHelper.Init();
              
            SystemServices systemServices=new SystemServices();

            systemServices.Init(Assembly.GetExecutingAssembly());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory(_container));

            var config = GlobalConfiguration.Configuration;
            config.Services.Replace(typeof(IHttpControllerActivator), new StructureMapApiControllerFactory(config,_container));
        

            CqrsCommandHandlerManager.Boot();
            CqrsEventHandlerManager.Boot();
            
        }
        
        private void AppDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
          
        }
    }
}
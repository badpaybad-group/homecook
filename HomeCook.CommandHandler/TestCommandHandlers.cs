using System;
using HomeCook.Command;
using HomeCook.Core.Cqrs;

namespace HomeCook.EntityCommandHandler
{
    public class TestCommandHandlers : ICommandHandler<TestCommand>
    {
        public void Handle(TestCommand cmd)
        {
            Console.WriteLine(cmd.ClientId+":: executing ::said:: "+ cmd.SayHello);
        }
    }
}
using HomeCook.Command;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;

namespace HomeCook.EntityCommandHandler
{
    /// <summary>
    /// with out using DDD, just command handle then access db by repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class AbstractEntityCommandHandlers<TEntity> : ICommandHandler<CreateEntity<TEntity>>
        ,ICommandHandler<UpdateEntity<TEntity>>,ICommandHandler<CreateOrUpdateEntity<TEntity>>,
        ICommandHandler<DeleteEntity<TEntity>>, ICommandHandler<DeleteEntityWithCondition<TEntity>> where TEntity:BaseEntity
    {
      
        private readonly ICommandRepository<TEntity> _repository;

        public AbstractEntityCommandHandlers()
        {   
            _repository = new MySqlCommandRepository<TEntity>();
        }
        
        public virtual void Handle(CreateEntity<TEntity> cmd)
        {
            _repository.Insert(cmd.Entity);
        }

        public virtual void Handle(UpdateEntity<TEntity> cmd)
        {
            _repository.Update(cmd.Entity);
        }

        public virtual void Handle(CreateOrUpdateEntity<TEntity> cmd)
        {
            _repository.InsertOrUpdate(cmd.Entity);
        }

        public virtual void Handle(DeleteEntity<TEntity> cmd)
        {
            _repository.Delete(cmd.Entity);
        }

        public virtual void Handle(DeleteEntityWithCondition<TEntity> cmd)
        {
            _repository.Delete(cmd.Predicate);
        }
    }
}
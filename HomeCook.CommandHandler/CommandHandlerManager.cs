using System;
using System.Collections.Generic;
using System.Linq;
using HomeCook.Core;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data.Entity;
using StructureMap;

namespace HomeCook.CommandHandler
{
    public static class CommandHandlerManager
    {
        static Dictionary<Type,ICommandHandler> _map=new Dictionary<Type, ICommandHandler>();
        private static Container _container;
        static CommandHandlerManager()
        {
        _container=new Container(r =>
        {
            r.AddRegistry<IocCoreRegister>();
            
        });
        }

        public static void Init()
        {
            Register(new EntityCommandHandler<FileInfo>());
            Register(new EntityCommandHandler<Right>());
            Register(new EntityCommandHandler<Role>());
            Register(new EntityCommandHandler<RoleRight>());
            Register(new EntityCommandHandler<UserInfo>());
            Register(new EntityCommandHandler<UserRole>());
        }
        public static List<Type> ListAllCommandHandler()
        {
            lock (_map)
            {
                var list = _map.Select(i => i.Key).ToList();
                return list;
            }
        }

        public static void Register(ICommandHandler cmdHandler)
        {
            lock (_map)
            {
                var type = cmdHandler.GetType();
                ICommandHandler handler;
                if (!_map.TryGetValue(type, out handler))
                {
                    _map[type] = cmdHandler;
                    cmdHandler.Register();
                }
            }
           
        }

        public static void Remove(Type cmdHandlerType)
        {
            lock (_map)
            {
                ICommandHandler handler;
                if (_map.TryGetValue(cmdHandlerType, out handler))
                {
                    handler.Unregister();
                    _map.Remove(cmdHandlerType);
                }
            }
          
        }

        public static void Remove(ICommandHandler cmdHandler)
        {
            lock (_map)
            {
                var cmdHandlerType = cmdHandler.GetType();
                ICommandHandler handler;
                if (_map.TryGetValue(cmdHandlerType, out handler))
                {
                    handler.Unregister();
                    _map.Remove(cmdHandlerType);
                }
            }
           
        }
    }
}
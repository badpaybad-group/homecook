﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using HomeCook.Command.Category;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;

namespace HomeCook.DomainCategory
{
    public class CategoryCommandHandler: ICommandHandler<CreateCategory>, ICommandHandler<UpdateCategory>
        ,ICommandHandler<DeleteCategory>
    {
        ICommandRepository<Category> _commandRepository=new MySqlCommandRepository<Category>();
        public void Handle(CreateCategory cmd)
        {
          _commandRepository.Insert(new Category()
          {
              Id= cmd.Id,
             
          });

            
        }

        public void Handle(UpdateCategory cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(DeleteCategory cmd)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Command.User;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Data.Entity;
using HomeCook.Core.Data.MySql;
using HomeCook.Core.Utility;
using HomeCook.Event.User;

namespace HomeCook.DomainUser
{
  public  class UserEventHandlers:IEventHandler<UserCreatedEvent>
    {
        public void Handle(UserCreatedEvent evt)
        {
            var uve = new UserVerify()
            {
                Id = evt.Id,
                Type = (int)UserVerify.UserVerifyType.Email,
                VerifyCode = evt.VerifyCode,
                VerifyValue = evt.Email
            };
            
            var uvp = new UserVerify()
            {
                Id = Guid.NewGuid(),
                Type = (int)UserVerify.UserVerifyType.Phone,
                VerifyCode = evt.VerifyCode,
                VerifyValue = evt.Phone
            };
            var u = new Core.Data.Entity.User()
            {
                Id = evt.Id,
                Username = evt.Username,
                Password = evt.Password
            };
           
        }

        /*
           public void Handle(ChangeUserPasswordCommand cmd)
      {
            using (var db = new HomeCookDbContext())
            {
                var u =
                    db.Users.SingleOrDefault(i => i.Username.Equals(cmd.Username, StringComparison.OrdinalIgnoreCase));
                if (u != null)
                {
                    u.Password = cmd.Password.ToPassword(cmd.Username);
                    db.SaveChanges();
                }
            }

        }

      public void Handle(ActiveUserCommand cmd)
      {
          var activeType = (int) UserVerify.UserVerifyType.Username;
          using (var db =new HomeCookDbContext())
          {
              var uv =
                  db.UserVerifies.FirstOrDefault(
                      i => i.Type==activeType &&
                          i.UserId == cmd.Id &&
                         i.VerifyValue.Equals(cmd.Username, StringComparison.OrdinalIgnoreCase) &&
                            i.VerifyCode.Equals(cmd.VerifyCode, StringComparison.OrdinalIgnoreCase));
              if (uv != null)
              {
                  uv.Verify = true;
                  var u = db.Users.FirstOrDefault(i => i.Id == cmd.Id);
                  if (u != null)
                  {
                      u.Actived = true;
                  }
                    db.SaveChanges();
              }
          }
      }

      public void Handle(DeactiveUserCommand cmd)
      {
          throw new NotImplementedException();
      }

      public void Handle(DeleteUserCommand cmd)
      {
          throw new NotImplementedException();
      }

      public void Handle(VerifyUserEmailCommand cmd)
      {
            var activeType = (int)UserVerify.UserVerifyType.Email;
            using (var db = new HomeCookDbContext())
            {
                var uv =
                    db.UserVerifies.FirstOrDefault(
                        i => i.Type == activeType &&
                            i.UserId == cmd.Id &&
                            i.VerifyValue.Equals(cmd.UserEmail, StringComparison.OrdinalIgnoreCase) &&
                            i.VerifyCode.Equals(cmd.VerifyCode, StringComparison.OrdinalIgnoreCase));
                if (uv != null)
                {
                    uv.Verify = true;
                    var u = db.Users.FirstOrDefault(i => i.Id == cmd.Id);
                    if (u != null)
                    {
                        u.Actived = true;
                    }
                    db.SaveChanges();
                }
            }
        }

      public void Handle(VerifyUserPhoneCommand cmd)
      {
            var activeType = (int)UserVerify.UserVerifyType.Email;
            using (var db = new HomeCookDbContext())
            {
                var uv =
                    db.UserVerifies.FirstOrDefault(
                        i => i.Type == activeType &&
                            i.UserId == cmd.Id &&
                            i.VerifyValue.Equals(cmd.UserPhone, StringComparison.OrdinalIgnoreCase) &&
                            i.VerifyCode.Equals(cmd.VerifyCode, StringComparison.OrdinalIgnoreCase));
                if (uv != null)
                {
                    uv.Verify = true;
                    var u = db.Users.FirstOrDefault(i => i.Id == cmd.Id);
                    if (u != null)
                    {
                        u.Actived = true;
                    }
                    db.SaveChanges();
                }
            }
        }

      public void Handle(SendPhoneActiveUser cmd)
      {
          throw new NotImplementedException();
      }

      public void Handle(SendEmailActiveUser cmd)
      {
          var title = "Active code is "+ cmd.VerifyCode;
          new SmptEmailHelper().SendLikeGmail(cmd.Email,"",title,
              "",false);
      }*/
    }
}

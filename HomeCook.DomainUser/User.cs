﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeCook.Core.Cqrs;
using HomeCook.Event.User;

namespace HomeCook.DomainUser
{
    public class User:AggregateRoot
    {
        public User()
        {
            
        }
        public static string GenerateVerifyCode()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode()).ToString();
        }

        public User(Guid id, string username, string password, string email, string phone)
        {
            DateTime createdOn=DateTime.Now;
            string verifyCode= GenerateVerifyCode();
            
            ApplyChange(new UserCreatedEvent(id, username, password,email, phone, createdOn, verifyCode));
        }

        public void Apply(UserCreatedEvent e)
        {
            Id = e.Id;
            _password = e.Password;
        }

        public void ChangePassword(string oldPassword, string password)
        {
            if (!oldPassword.Equals(_password))
            {
                throw new Exception("No match password");
            }

            DateTime updatedOn=DateTime.Now;
            Guid updatedBy= Id;
            ApplyChange(new UserChangedPasswordEvent(Id,password, updatedOn,updatedBy));
        }

        private string _password=string.Empty;
        public void Apply(UserChangedPasswordEvent e)
        {
            _password = e.Password;
        }
    }

}

﻿using System;
using System.Linq;
using HomeCook.Command.User;
using HomeCook.Core.Cqrs;
using HomeCook.Core.Cqrs.Data;

namespace HomeCook.DomainUser
{
   
    public  class UserCommandHandlers: ICommandHandler<CreateUserCommand>,ICommandHandler<ChangeUserPasswordCommand>
        ,ICommandHandler<ActiveUserCommand>, ICommandHandler<DeactiveUserCommand>
        ,ICommandHandler<DeleteUserCommand>
        , ICommandHandler<VerifyUserEmailCommand>,ICommandHandler<VerifyUserPhoneCommand>
        ,ICommandHandler<SendPhoneActiveUser>,ICommandHandler<SendEmailActiveUser>
    {
      
        ICqrsEventSourcingRepository<User> _repository=new CqrsEventSourcingRepository<User>();

      public void Handle(CreateUserCommand cmd)
      {
           
          var user=new User(cmd.Id, cmd.Username,cmd.Password, cmd.Email,cmd.Phone);
            //_repository.Save(user,-1);
          _repository.CreateNew(user);
        }


        public void Handle(ChangeUserPasswordCommand cmd)
        {
            _repository.Save(cmd.Id, user =>
            {
                //list action for domain user need to invoke
                user.ChangePassword(cmd.OldPassword, cmd.Password);
            });
            //var user = _repository.Get(cmd.Id);
            //user.ChangePassword(cmd.OldPassword, cmd.Password);
            //_repository.Save(user,-1);
        }

        public void Handle(ActiveUserCommand cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(DeactiveUserCommand cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(DeleteUserCommand cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(VerifyUserEmailCommand cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(VerifyUserPhoneCommand cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(SendPhoneActiveUser cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(SendEmailActiveUser cmd)
        {
            throw new NotImplementedException();
        }
    }
}

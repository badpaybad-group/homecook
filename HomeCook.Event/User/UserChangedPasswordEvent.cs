﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Event.User
{
    public class UserChangedPasswordEvent : BaseEvent
    {
        public string Password { get; }
        public DateTime UpdatedOn { get; }
        public Guid UpdatedBy { get; }

        public UserChangedPasswordEvent(Guid id, string password, DateTime updatedOn, Guid updatedBy):base(id)
        {
            Password = password;
            UpdatedOn = updatedOn;
            UpdatedBy = updatedBy;
           
        }
    }
}
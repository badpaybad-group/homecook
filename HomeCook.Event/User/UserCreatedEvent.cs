﻿using System;
using HomeCook.Core.Cqrs;

namespace HomeCook.Event.User
{
    public class UserCreatedEvent : BaseEvent
    {
        public string Username { get; }
        public string Password { get; }
        public string Email { get; }
        public string Phone { get; }
        public DateTime CreatedOn { get; }
        public string VerifyCode { get; }

        public UserCreatedEvent(Guid id, string username, string password, string email, string phone, DateTime createdOn, string verifyCode) : base(id)
        {
            Username = username;
            Password = password;
            Email = email;
            Phone = phone;
            CreatedOn = createdOn;
            VerifyCode = verifyCode;
        }
    }
}

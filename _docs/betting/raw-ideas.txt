﻿1. Conent betting (nội dung)
Nội dung để người chơi tham gia
Nội dung được tạo ra bởi 1 tài khoản bất kỳ, hoặc tài khoản hệ thống
Nội dung có thể là live stream, ảnh, video, một bài viết, câu hỏi
Nội dung sẽ tạo ra các betting, tạo các kèo theo thời gian
Nội dung có thời gian bắt đầu và kết thúc cụ thể

2. Betting (kèo)
Các kèo trong nội dung được tạo bởi chủ nội dung
Để mô tả từng thời điểm phát sinh ra sự kiện cho người chơi chọn các betting item (kết quả kèo)
Các kèo nằm trong khoảng thời gian của nội dung
Cho phép người chơi chọn một hoặc nhiều kết quả có thể xảy ra
Các trạng thái: Ẩn, Chờ, mở để chọn kết quả kèo, đã mở, chuẩn bị đóng, đóng, đã có kết quả, hủy
Người chủ nội dung sẽ cập nhật và công bố các "kết quả kèo" nào là thắng, hoặc xảy ra

3. Betting Item (kết quả kèo)
Các kết quả có thể cho người chơi lựa chọn cho kèo
Các kết quả được tạo ra bởi chủ nội dung kèo

4. Betting transaction
Ghi nhận các lựa chọn kết quả kèo của người dùng 
Khi người dùng chọn, hệ thống sẽ ngậm kèo và xử lý đủ điều kiện sẽ trả ra là nhận kèo, không thì từ chối

Khi nhận kèo (kết quả kèo), sẽ dựa vào trạng thái kèo để xử lý kết quả.
Khi kèo đã có kết quả các transaction sẽ được xử lý trả lại trạng thái win,lose, ...

5. Hệ thống tích điểm, tặng quà cho người chơi
